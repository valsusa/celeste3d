      subroutine tridiagv(nqi, i1, i2, bcr, srite, a, b, c, d, e, f, s) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nqi 
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      real(double) , intent(in) :: bcr 
      real(double) , intent(in) :: srite 
      real(double) , intent(in) :: a(*) 
      real(double) , intent(in) :: b(*) 
      real(double) , intent(in) :: c(*) 
      real(double) , intent(in) :: d(*) 
      real(double) , intent(inout) :: e(*) 
      real(double) , intent(inout) :: f(*) 
      real(double) , intent(inout) :: s(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i, j 
      real(double) :: rws 
!-----------------------------------------------
!
!=======================================================================
!
      do i = i1, i2, nqi 
         rws = 1./(b(i)-c(i)*e(i-nqi)) 
         e(i) = a(i)*rws 
         f(i) = (d(i)+c(i)*f(i-nqi))*rws 
      end do 
!
!-----------------------------------------------------------------------
!
!     solve for s(i2)
!
      if (bcr == 0.0) then 
         s(i2) = e(i2)*srite + f(i2) 
      else 
         rws = 1./(1. - e(i2)) 
         s(i2) = (e(i2)*srite+f(i2))*rws 
      endif 
!
!-----------------------------------------------------------------------
!
      j = i2 - nqi 
      do i = i1, i2 - nqi, nqi 
         s(j) = e(j)*s(j+nqi) + f(j) 
         j = j - nqi 
      end do 
!
!-----------------------------------------------------------------------
!
      return  
      end subroutine tridiagv 
