      module controllo_M 
      
      integer, parameter :: ntmp = 1280 

      integer , dimension(8) :: nvlist 
      integer , dimension(8,ntmp) :: lvlist 
      integer , dimension(ntmp) :: itmp, nploc 
      integer :: cvmbp, np1, ipdone, np2, np, lvmax, ntimes, ii, lv, ix, iy, iz&
         , lismax, kmax, jmax, imax, lv1, n1, lv2, n2, npn, ipjk, ipjpk, ijpk, &
         ijkp, ipjkp, ijpkp, ipjpkp 
      real , dimension(ntmp) :: wdif 
      
      end module controllo_M