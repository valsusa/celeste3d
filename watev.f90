      subroutine watev(ncells, ijkcell, iphead, itdim, pxi, peta, pzta, wate) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: ncells 
      integer , intent(in) :: itdim 
      integer , intent(in) :: ijkcell(*) 
      integer , intent(in) :: iphead(*) 
      real(double) , intent(in) :: pxi(0:*) 
      real(double) , intent(in) :: peta(0:*) 
      real(double) , intent(in) :: pzta(0:*) 
      real(double) , intent(out) :: wate(itdim,*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: n, ijk, inew, jnew, knew 
      real(double) :: pxi2, peta2, pzta2, wi, wip, wj, wjp, wk, wkp 
!-----------------------------------------------
!
      do n = 1, ncells 
!
         ijk = ijkcell(n) 
!
         inew = int(pxi(iphead(ijk))) 
         jnew = int(peta(iphead(ijk))) 
         knew = int(pzta(iphead(ijk))) 
!
!
         pxi2 = pxi(iphead(ijk)) - float(inew) 
         peta2 = peta(iphead(ijk)) - float(jnew) 
         pzta2 = pzta(iphead(ijk)) - float(knew) 
!
!     calculate interpolation weights
!
         wi = 1. - pxi2 
         wip = pxi2 
!
         wj = 1. - peta2 
         wjp = peta2 
!
         wk = 1. - pzta2 
         wkp = pzta2 
!
         wate(ijk,1) = wip*wj*wk 
         wate(ijk,2) = wip*wjp*wk 
         wate(ijk,3) = wi*wjp*wk 
         wate(ijk,4) = wi*wj*wk 
!
         wate(ijk,5) = wip*wj*wkp 
         wate(ijk,6) = wip*wjp*wkp 
         wate(ijk,7) = wi*wjp*wkp 
         wate(ijk,8) = wi*wj*wkp 
!
      end do 
!
 
      return  
      end subroutine watev 


 
 
 
      subroutine watevtmp(ncells, ijkcell, iphead, itdim, pxi, peta, pzta, wate&
         ) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: ncells 
      integer , intent(in) :: itdim 
      integer , intent(in) :: ijkcell(*) 
      integer , intent(in) :: iphead(*) 
      real(double) , intent(in) :: pxi(0:*) 
      real(double) , intent(in) :: peta(0:*) 
      real(double) , intent(in) :: pzta(0:*) 
      real(double) , intent(out) :: wate(itdim,*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: n, ijk, inew, jnew, knew 
      real(double) :: pxi2, peta2, pzta2, wi, wip, wj, wjp, wk, wkp 
!-----------------------------------------------
!
      do n = 1, ncells 
!
         ijk = ijkcell(n) 
!
         inew = int(pxi(iphead(n))) 
         jnew = int(peta(iphead(n))) 
         knew = int(pzta(iphead(n))) 
!
!
         pxi2 = pxi(iphead(n)) - float(inew) 
         peta2 = peta(iphead(n)) - float(jnew) 
         pzta2 = pzta(iphead(n)) - float(knew) 
!
!     calculate interpolation weights
!
         wi = 1. - pxi2 
         wip = pxi2 
!
         wj = 1. - peta2 
         wjp = peta2 
!
         wk = 1. - pzta2 
         wkp = pzta2 
!
         wate(n,1) = wip*wj*wk 
         wate(n,2) = wip*wjp*wk 
         wate(n,3) = wi*wjp*wk 
         wate(n,4) = wi*wj*wk 
!
         wate(n,5) = wip*wj*wkp 
         wate(n,6) = wip*wjp*wkp 
         wate(n,7) = wi*wjp*wkp 
         wate(n,8) = wi*wj*wkp 
!
      end do 
!
 
      return  
      end subroutine watevtmp 
