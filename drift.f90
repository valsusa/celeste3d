      subroutine exbdrift(nvtx, ijkvtx, ex, ey, ez, bxv, byv, bzv, udrft, vdrft&
         , wdrft) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nvtx 
      integer , intent(in) :: ijkvtx(*) 
      real(double) , intent(in) :: ex(*) 
      real(double) , intent(in) :: ey(*) 
      real(double) , intent(in) :: ez(*) 
      real(double) , intent(in) :: bxv(*) 
      real(double) , intent(in) :: byv(*) 
      real(double) , intent(in) :: bzv(*) 
      real(double) , intent(out) :: udrft(*) 
      real(double) , intent(out) :: vdrft(*) 
      real(double) , intent(out) :: wdrft(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: n, ijk 
      real(double) :: rbsq 
!-----------------------------------------------
!
!     a routine to calculate the E X B drift
!
!
      do n = 1, nvtx 
!
         ijk = ijkvtx(n) 
!
         rbsq = 1./(bxv(ijk)**2+byv(ijk)**2+bzv(ijk)**2+1.E-10) 
!
         udrft(ijk) = (ey(ijk)*bzv(ijk)-ez(ijk)*byv(ijk))*rbsq 
         vdrft(ijk) = (ez(ijk)*bxv(ijk)-ex(ijk)*bzv(ijk))*rbsq 
         wdrft(ijk) = (ex(ijk)*byv(ijk)-ey(ijk)*bxv(ijk))*rbsq 
!
      end do 
!
      return  
      end subroutine exbdrift 
