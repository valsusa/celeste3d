      subroutine jhalf 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use corgan_com_M 
      use cindex_com_M 
      use numpar_com_M 
      use cprplt_com_M, ONLY:  
      use cophys_com_M 
      use ctemp_com_M, ONLY: fmf, fef, fvf, fbxf, fbyf, fbzf, twx 
      use blcom_com_M, ONLY: qom, wate, qdnv, jxtilde, jytilde, jztilde, j12x, &
         j12y, j12z, decyk, jdote, bxv, byv, bzv, gradxb, gradyb, gradzb, a11, &
         a12, a13, itmax, itsub, a21, a22, a23, phipot, exold, eyold, ezold, &
         ex, ey, ez, vol, vvol, vvol_half, ijkcell, ijkvtx, c1x, c2x, c3x, &
         c4x, c5x, c6x, c7x, c8x, c1y, c2y, c3y, c4y, c5y, c6y, c7y, c8y, c1z, &
         c2z, c3z, c4z, c5z, c6z, c7z, c8z, wkl, wkr, wke, wkf, periodicx 
      use objects_com_M, ONLY:  
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer , dimension(8) :: iv 
      integer , dimension(4) :: lioinput, lioprint 
      integer :: n 
      real(double), dimension(1) :: ixi1, ixi2, ieta1, ieta2 
      real(double) :: ixi4, ieta4, ixi5, ieta5 
      real(double), dimension(8) :: wght 
      real(double), dimension(100) :: fpxf, fpyf, fpzf, fpxft, fpyft, fpzft, &
         ucm, vcm, wcm, cm 
      real(double), dimension(3,20) :: wsin, wcos 
      real(double), dimension(3,12,20) :: tsi, rot 
      real(double) :: transpos, dot, dummy, error, tiny, tee0, tee1 
      logical :: expnd, nomore 
      character :: name*80 
!-----------------------------------------------
!
      call mudote (nvtx, ijkvtx, nsp, itdim, qdnv, qom, bxv, byv, bzv, dt, &
         transpos, clite, ex, ey, ez, a11, a12, a13) 
!
      do n = 1, nvtx 
         ijk = ijkvtx(n) 
         j12x(ijk) = jxtilde(ijk) + dt*0.5*a11(ijk) 
         j12y(ijk) = jytilde(ijk) + dt*0.5*a12(ijk) 
         j12z(ijk) = jztilde(ijk) + dt*0.5*a13(ijk) 
!     j12x(ijk)=jxtilde(ijk)
!     j12y(ijk)=jytilde(ijk)
!     j12z(ijk)=jztilde(ijk)
         write (88, *) ijk, j12x(ijk) 
      end do 
!     to cut tests out, return here
      return  
!      dot=dot+decyk(ijk)*vol(ijk)
      end subroutine jhalf 
