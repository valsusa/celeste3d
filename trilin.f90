!dk trilin
      subroutine trilin(ncells, ijkcell, itdim, iwid, jwid, kwid, wate, bxv, &
         byv, bzv, bxpn, bypn, bzpn) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: ncells 
      integer , intent(in) :: itdim 
      integer , intent(in) :: iwid 
      integer , intent(in) :: jwid 
      integer , intent(in) :: kwid 
      integer , intent(in) :: ijkcell(*) 
      real(double) , intent(in) :: wate(itdim,*) 
      real(double) , intent(in) :: bxv(*) 
      real(double) , intent(in) :: byv(*) 
      real(double) , intent(in) :: bzv(*) 
      real(double) , intent(out) :: bxpn(*) 
      real(double) , intent(out) :: bypn(*) 
      real(double) , intent(out) :: bzpn(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: n, ijk 
!-----------------------------------------------
!
!dir$ ivdep
      do n = 1, ncells 
!
         ijk = ijkcell(n) 
!
!
         bxpn(ijk) = wate(ijk,1)*bxv(ijk+iwid) + (wate(ijk,2)*bxv(ijk+iwid+jwid&
            )+(wate(ijk,3)*bxv(ijk+jwid)+(wate(ijk,4)*bxv(ijk)+(wate(ijk,5)*bxv&
            (ijk+iwid+kwid)+(wate(ijk,6)*bxv(ijk+iwid+jwid+kwid)+(wate(ijk,7)*&
            bxv(ijk+jwid+kwid)+wate(ijk,8)*bxv(ijk+kwid))))))) 
!
         bypn(ijk) = wate(ijk,1)*byv(ijk+iwid) + (wate(ijk,2)*byv(ijk+iwid+jwid&
            )+(wate(ijk,3)*byv(ijk+jwid)+(wate(ijk,4)*byv(ijk)+(wate(ijk,5)*byv&
            (ijk+iwid+kwid)+(wate(ijk,6)*byv(ijk+iwid+jwid+kwid)+(wate(ijk,7)*&
            byv(ijk+jwid+kwid)+wate(ijk,8)*byv(ijk+kwid))))))) 
!
         bzpn(ijk) = wate(ijk,1)*bzv(ijk+iwid) + (wate(ijk,2)*bzv(ijk+iwid+jwid&
            )+(wate(ijk,3)*bzv(ijk+jwid)+(wate(ijk,4)*bzv(ijk)+(wate(ijk,5)*bzv&
            (ijk+iwid+kwid)+(wate(ijk,6)*bzv(ijk+iwid+jwid+kwid)+(wate(ijk,7)*&
            bzv(ijk+jwid+kwid)+wate(ijk,8)*bzv(ijk+kwid))))))) 
!
      end do 
!
      return  
      end subroutine trilin 
