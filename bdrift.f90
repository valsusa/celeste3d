      subroutine bdrift(ncells, ijkcell, nsp, itdim, iwid, jwid, kwid, qdnv, &
         qom, bxn, byn, bzn, dt, clite, alfabx, alfaby, alfabz) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: ncells 
      integer , intent(in) :: nsp 
      integer , intent(in) :: itdim 
      integer , intent(in) :: iwid 
      integer , intent(in) :: jwid 
      integer , intent(in) :: kwid 
      real(double) , intent(in) :: dt 
      real(double) , intent(in) :: clite 
      integer , intent(in) :: ijkcell(*) 
      real(double) , intent(in) :: qdnv(itdim,*) 
      real(double) , intent(in) :: qom(*) 
      real(double) , intent(in) :: bxn(*) 
      real(double) , intent(in) :: byn(*) 
      real(double) , intent(in) :: bzn(*) 
      real(double) , intent(inout) :: alfabx(*) 
      real(double) , intent(inout) :: alfaby(*) 
      real(double) , intent(inout) :: alfabz(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: n, ijk, is 
      real(double) :: beta, qc, denom 
!-----------------------------------------------
!
!     a routine to calculate mu*b
!
!
!
      alfabx(ijkcell(:ncells)) = 0.0 
      alfaby(ijkcell(:ncells)) = 0.0 
      alfabz(ijkcell(:ncells)) = 0.0 
!
!
      do is = 1, nsp 
!
         beta = 0.5*qom(is)*dt/clite 
!
         do n = 1, ncells 
!
            ijk = ijkcell(n) 
!
            qc = 0.125*(qdnv(ijk+iwid,is)+qdnv(ijk+iwid+jwid,is)+qdnv(ijk+jwid,&
               is)+qdnv(ijk,is)+qdnv(ijk+iwid+kwid,is)+qdnv(ijk+iwid+jwid+kwid,&
               is)+qdnv(ijk+jwid+kwid,is)+qdnv(ijk+kwid,is)) 
!
            denom = 1./(1. + beta**2*(bxn(ijk)**2+byn(ijk)**2+bzn(ijk)**2)) 
!
            alfabx(ijk) = alfabx(ijk) + qom(is)*qc*bxn(ijk)*denom 
!
            alfaby(ijk) = alfaby(ijk) + qom(is)*qc*byn(ijk)*denom 
!
            alfabz(ijk) = alfabz(ijk) + qom(is)*qc*bzn(ijk)*denom 
!
!
         end do 
      end do 
!
      return  
      end subroutine bdrift 
