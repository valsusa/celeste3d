      subroutine absolute(ncells, ijkcell, bx, by, bz, absb) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: ncells 
      integer , intent(in) :: ijkcell(*) 
      real(double) , intent(in) :: bx(*) 
      real(double) , intent(in) :: by(*) 
      real(double) , intent(in) :: bz(*) 
      real(double) , intent(out) :: absb(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: n, ijk 
!-----------------------------------------------
!
!
      do n = 1, ncells 
!
         ijk = ijkcell(n) 
!
         absb(ijk) = sqrt(bx(ijk)**2+by(ijk)**2+bz(ijk)**2) 
!
      end do 
!
      return  
      end subroutine absolute 
