!dk celindex
      subroutine celindex(i1, i2, j1, j2, k1, k2, iwid, jwid, kwid, ijkcell, &
         ijkctmp, ncells, ijkvtx, nvtxkm, nvtx) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      integer , intent(in) :: j1 
      integer , intent(in) :: j2 
      integer , intent(in) :: k1 
      integer , intent(in) :: k2 
      integer , intent(in) :: iwid 
      integer , intent(in) :: jwid 
      integer , intent(in) :: kwid 
      integer , intent(out) :: ncells 
      integer , intent(out) :: nvtxkm 
      integer , intent(out) :: nvtx 
      integer , intent(inout) :: ijkcell(*) 
      integer , intent(out) :: ijkctmp(*) 
      integer , intent(out) :: ijkvtx(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: k, j, i 
!-----------------------------------------------
!
!
      ncells = 0 
!
      do k = k1, k2 
         do j = j1, j2 
            do i = i1, i2 
!
               ncells = ncells + 1 
!
               ijkcell(ncells) = 1 + (i - 1)*iwid + (j - 1)*jwid + (k - 1)*kwid 
               ijkctmp(ncells) = ijkcell(ncells) 
!
            end do 
         end do 
      end do 
!
!
      nvtx = 0 
!
      do k = k1, k2 
         do j = j1, j2 + 1 
            do i = i1, i2 + 1 
!
               nvtx = nvtx + 1 
!
               ijkvtx(nvtx) = 1 + (i - 1)*iwid + (j - 1)*jwid + (k - 1)*kwid 
!
            end do 
         end do 
      end do 
!
      nvtxkm = nvtx 
!
      do j = j1, j2 + 1 
         do i = i1, i2 + 1 
!
            nvtx = nvtx + 1 
!
            ijkvtx(nvtx) = 1 + (i - 1)*iwid + (j - 1)*jwid + k2*kwid 
!
         end do 
      end do 
      return  
      end subroutine celindex 
