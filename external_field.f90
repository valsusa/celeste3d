subroutine external_field      
USE vast_kind_param
use corgan_com_M 
use cindex_com_M 
use numpar_com_M 
use cprplt_com_M
use cophys_com_M 
use ctemp_com_M 
use blcom_com_M         
implicit none
real(8) :: a_m,o_m,g_x,v_int,bavg2,vavg,timescale,z_c
integer :: n,navg

timescale=bx_ini
! the standard for the Newton challenge is a_m=2
!a_m=2.d0*1.5d0
a_m=ey_ext
o_m=.05d0*timescale

vavg=0.d0
navg=0
z_c=kbar*dz/2.d0
do n=1,nvtx
   ijk=ijkvtx(n)
   if(abs(z(ijk)-z_c).gt.6.d0) then
   bavg2=bxv(ijk)**2+byv(ijk)**2+bzv(ijk)**2
   g_x=sin(pi*x(ijk)/dx/dble(ibar))**2
   v_int=2.d0*a_m*o_m*tanh(o_m*t)/cosh(o_m*t)**2
if (shock) then
!  for shock problem uncomment the following two lines
   g_x=1d0
   v_int=.1d0
!  end special lines for shock problem
end if
   ey(ijk)=ey(ijk)+v_int*g_x*bavg2/(abs(bxv(ijk))+1.d-10)
   vavg=vavg+v_int*g_x*bavg2/(abs(bxv(ijk))+1.d-10)
   navg=navg+1
   end if
enddo
write(*,*)'INFLOW SPEED=',vavg/navg

end subroutine external_field
