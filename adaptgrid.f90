      subroutine adaptgrid(j1, j2, a, b, c, d, e, f, w, x, xn, bcl, bcr, xbcl, &
         xbcr) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: j1 
      integer  :: j2 
      real(double) , intent(in) :: bcl 
      real(double)  :: bcr 
      real(double) , intent(in) :: xbcl 
      real(double)  :: xbcr 
      real(double)  :: a(*) 
      real(double)  :: b(*) 
      real(double)  :: c(*) 
      real(double)  :: d(*) 
      real(double)  :: e(*) 
      real(double)  :: f(*) 
      real(double) , intent(in) :: w(*) 
      real(double) , intent(in) :: x(*) 
      real(double)  :: xn(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: j 
      real(double) :: tiny 
!-----------------------------------------------
!
!=======================================================================
!
!      a routine to generate an adaptive grid
!     the generator equation is:
!
!      tiny*(xn(j)-x(j))-0.5*(w(j)+w(j-1))*(xn(j+1)-2.*xn(j)+xn(j-1))
!    &     =-0.5*(w(j)-w(j-1))*(xn(j+1)-xn(j-1))
!
!     tiny is intended to be a small number to prevent
!     singularity when w=0.
!
!     bcl(bcr)=0 Dirichlet and xbcl(xbcr) is the boundary
!     point position
!
!     bcl(bcr)=1 Neumann and xbcl(xbcr) is the boundary dx
!
!=======================================================================
!
!     This routine is called by CELEST1D
!
!     This routine calls TRIDIAGV
!
!=======================================================================
!
      xn(j1) = x(j1) 
      xn(j2) = x(j2) 
!
!-----------------------------------------------------------------------
!
      tiny = 2.0 
!
!-----------------------------------------------------------------------
!
      a(j1:j2) = w(j1:j2) 
      b(j1:j2) = w(j1:j2) + w(j1-1:j2-1) + tiny 
      c(j1:j2) = w(j1-1:j2-1) 
      d(j1:j2) = tiny*x(j1:j2) 
      xn(j1:j2) = x(j1:j2) 
!
!-----------------------------------------------------------------------
!
      if (bcl == 0.0) then 
         e(j1-1) = 0.0 
         f(j1-1) = xbcl 
      else 
         e(j1-1) = 1.0 
         f(j1-1) = -xbcl 
      endif 
      call tridiagv (1, j1, j2, bcr, xbcr, a, b, c, d, e, f, xn) 
!
!=======================================================================
!
      return  
      end subroutine adaptgrid 
