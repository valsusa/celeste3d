ARCH =gfortran
                                                                                
ifeq ($(ARCH),gfortran)
                                                                                
#FRANKLIN PGI
F90= gfortran
LD= gfortran
LDFLAGS = -L/usr/local/lib -L/usr/lib -L/usr/local/Cellar/netcdf-fortran/4.6.1/lib -L /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/lib
FFLAGS =       -O -fdefault-real-8 -fdefault-double-8 -fallow-argument-mismatch
#-fno-underscoring
endif

ifeq ($(ARCH),linux)
                                                                                
#LAHEY - linux
F90= lf95
LD= lf95
FFLAGS =       -O --dbl
endif                                                                                

ifeq ($(ARCH),intel)

#INTEL 
F90= ifort
LD= ifort
LDFLAGS = #-pg
FFLAGS =  -nowarn  -O3 -r8  
#-check all
                 
endif
    
ifeq ($(ARCH),g95)

#GNU G95
F90= g95
LD= g95
LDFLAGS =    -L/usr/lib 
FFLAGS =   -O3 -r8  -fno-second-underscore
#FFLAGS =   -r8 -O3

endif 

ifeq ($(ARCH),mac)                                                                     
#ABSOFT - MAC
F90= f95
LD= f95
LDFLAGS = -P
FFLAGS =  -w  -O3 -N113  -YEXT_NAMES=LCS -YEXT_SFX=_ 
#FFLAGS =  -w  -Rs -N113  -YEXT_NAMES=LCS -YEXT_SFX=_ 
                                                               
endif

#lf95 lahey computers
#LIBS = libnetcdf.a
# G4 laptop
#LIBS = /Users/lapenta/Desktop/stuff/netcdf-3.6.0-p1/lib/libnetcdf.a
# LIBS = /Users/administrator/Documents/fortran/netcdf-3.6.1/lib/libnetcdf.a /usr/lib/libSystemStubs.a
#for laptop MAC Intel
#LIBS = /Users/gianni/Documents/fortran/netcdf-3.6.1/lib/libnetcdf.a  /Developer/SDKs/MacOSX10.4u.sdk/usr/lib/libSystemStubs.a
#for office pc in leuven
#LIBS = /usr/local/netcdf/lib/libnetcdf.a
# For IGPP MACS use below
#LIBS=/sw/lib/libnetcdf.a
# For ulisse
#LIBS=/usr/lib/libnetcdf.a
#For CLUSTER KU LEUVEN
#LIBS=/data/home/u0052182/netcdf-3.6.1/lib/libnetcdf.a
#LIBS=/data/home/u0052182/libnetcdf.a
# For new computer office KU Leuven
#LIBS= /home/gianni/netcdf-3.6.2/lib/libnetcdf.a
#LIBS= $(NETCDF)

# Officeapple 
#LIBS = /usr/local/lib/libnetcdf.a

LIBS = -lnetcdff
#LIBS = /usr/local/Cellar/netcdf/4.9.2_1/lib/libnetcdf.a

CMD	=	xcel
 
# Profiling: put -P in FFLAGS AND LDFLAGS

# derf.f  needs to be commented out in the gfortran compilation
SRCSF= #derf.f
SRCS=	vast_kind_param_M.f90 corgan_com_M.f90 \
	blcom_com_M.f90     ctemp_com_M.f90   numpar_com_M.f90 \
	cindex_com_M.f90  cplot_com_M.f90   modify_com_M.f90  objects_com_M.f90 \
	cophys_com_M.f90  cprplt_com_M.f90  boundary_M.f90 netcdf_M.f90  controllo_M.f90 moduli.f90  \
	absolute.f90 adaptgrid.f90 bdrift.f90 budget.f90 celdex.f90 \
	celeste3dnew.f90 celindex.f90 celstep.f90 chnglst.f90 counter.f90 \
	drift.f90 gauss3v.f90 geom.f90 bcgeom.f90 gmres.f90\
	gridinit_belt.f90 gridinit_cart.f90 initiv_wall.f90 map3d.f90 map3d_axis.f90 map3d_surf.f90 \
	metric.f90 metricc.f90 \
	gridinit.f90 jet.f90  \
	mshset.f90   \
	parcelc.f90 parcelv.f90 parcelv_obj.f90  \
	parcelc_obj.f90 parmovgc_wall.f90 parmovi_rel.f90 \
	parmovi_wall.f90 parset_reg.f90 parset_obj.f90 partcles_wall.f90 \
	locat_box.f90 parlocat_all_wall.f90  parlocat_all_wall_obj.f90   parvol.f90 \
	parrefl_diffuse.f90 rotate.f90 s.f90  parrefl.f90 \
	spline.f90 splint.f90 trilin.f90 triple.f90 triquad.f90 vinit.f90 \
	smooth.f90 vtoctov.f90 volume_vtx.f90 vtxb.f90 \
	vtxindx.f90 watec.f90 watev.f90 weights.f90 \
	toroidal_coords.f90  tridiagv.f90 fft2.f90 fft_web.f90 \
	svector.f90 sgrid3d.f90  test_box.f90 dummy.f90 finito.f90 current.f90 \
	fft1.f90 e0.f90 jpl.f90 boundary.f90 ay.f90 poisson.f90 poisson_new.f90\
	linux.f90 fuvector.f90  maxwell.f90\
	parinject.f90 rinj.f90 generate.f90 external_field.f90 restart_new.f90 \
	control.f90 ncdlib3.f90 dfftpack.f90 initial_state.f90 work.f90 fields.f90 #erf.f90

 
OBJECTS = $(SRCS:%.f90=%.o) $(SRCSF:%.f=%.o)

.SUFFIXES:
.SUFFIXES: .o .f90 .f

.PHONY: clean tar

.f90.o:
	$(F90) -c $(FFLAGS) $<

.f.o:
	$(F90) -c $(FFLAGS) $<

xcel:   $(OBJECTS)
#	$(F90) -c -O3  -fno-second-underscore ncdlib3.f90 dfftpack.f90
#	$(LD) $(LDFLAGS) -o xcel $(OBJECTS) ncdlib3.o dfftpack.o $(LIBS)
	$(LD) $(LDFLAGS) -o xcel $(OBJECTS)   $(LIBS)
clean:
	rm -f xcel *.o *.mod core fort.* *.dat outto
	del  xcel *.o *.mod core fort.* *.dat outto

