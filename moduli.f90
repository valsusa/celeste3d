module celeste3d
     USE vast_kind_param, ONLY:  double
implicit none
!      integer , dimension(8) :: iv
      integer :: n, nsm, ll, nn, iter
      real(double), dimension(1) :: ixi1, ixi2, ieta1, ieta2
      real(double) :: ixi4, ieta4, ixi5, ieta5
      real(double), dimension(8) :: wght
      real(double), dimension(100) :: fpxf, fpyf, fpzf, fpxft, fpyft, fpzft, &
         ucm, vcm, wcm, cm
      real(double), dimension(3,20) :: wsin, wcos
      real(double), dimension(3,12,20) :: tsi, rot
      real(double) :: sok, sko, cok, cko, celplt, wk, absbc, rvvol, where, &
         transpos, trl, tmin, told, xx, t2old, error, relax, tiny, dumdt, phibc&
         , dumscal, ws, errormaxwell, t3
      logical :: expnd, nomore, firstime
      real(double) :: dnorm,ez_avg
end module celeste3d
