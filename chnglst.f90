      subroutine chnglst(np, iphed0, iphed1, link) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: np 
      integer , intent(out) :: iphed0 
      integer , intent(inout) :: iphed1 
      integer , intent(inout) :: link 
!-----------------------------------------------
      iphed0 = link 
      link = iphed1 
      iphed1 = np 
!
      return  
      end subroutine chnglst 
