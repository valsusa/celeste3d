!dk rotate
      subroutine rotate(ibar, jbar, kbar, rmaj, dz, strait, iota, istep, delt, &
         dtheta, dzstr, dphi, cdlt, sdlt, cdlhf, sdlhf, cdph, sdph, cdphhf, &
         sdphhf) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: ibar 
      integer , intent(in) :: jbar 
      integer , intent(in) :: kbar 
      real(double) , intent(in) :: rmaj 
      real(double) , intent(in) :: dz 
      real(double) , intent(in) :: strait 
      real(double) , intent(out) :: delt 
      real(double) , intent(out) :: dtheta 
      real(double) , intent(out) :: dzstr 
      real(double) , intent(out) :: dphi 
      real(double) , intent(out) :: cdlt 
      real(double) , intent(out) :: sdlt 
      real(double) , intent(out) :: cdlhf 
      real(double) , intent(out) :: sdlhf 
      real(double) , intent(out) :: cdph 
      real(double) , intent(out) :: sdph 
      real(double) , intent(out) :: cdphhf 
      real(double) , intent(out) :: sdphhf 
      integer , intent(out) :: istep(*) 
      real(double) , intent(in) :: iota(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: k 
      real(double) :: pi, rfibar, rfjbar 
!-----------------------------------------------
!     calculate elements of rotation matrix for toroidal direction
!
      pi = acos(-1.) 
!
      rfibar = 1./float(ibar) 
      rfjbar = 1./float(jbar) 
!
      dtheta = 2.*pi*rfibar 
      delt = 0.0 
      if (rmaj > 0.0) delt = dz/rmaj 
      dphi = delt*rfjbar 
      dzstr = strait*dz*rfjbar 
!
      cdlt = cos(delt) 
      sdlt = sin(delt) 
      cdlhf = sqrt(0.5*(1. + cdlt)) 
      sdlhf = sqrt(0.5*(1. - cdlt)) 
      cdph = cos(dphi) 
      sdph = sin(dphi) 
      cdphhf = cos(dphi/2.) 
      sdphhf = sin(dphi/2.) 
!
!
!     define for quasi-ballooning coordinates
!
      do k = 1, kbar + 2 
!
         istep(k) = ifix(iota(k)*ibar+0.5)*delt/(2.*pi) 
!
      end do 
!
      return  
      end subroutine rotate 
