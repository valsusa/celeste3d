 
!dk vtxb
      subroutine vtxb(i1, i2, j1, j2, k1, k2, iwid, jwid, kwid, vol, bxn, byn, &
         bzn, bxv, byv, bzv) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      integer , intent(in) :: j1 
      integer , intent(in) :: j2 
      integer , intent(in) :: k1 
      integer , intent(in) :: k2 
      integer , intent(in) :: iwid 
      integer , intent(in) :: jwid 
      integer , intent(in) :: kwid 
      real(double) , intent(in) :: vol(*) 
      real(double) , intent(in) :: bxn(*) 
      real(double) , intent(in) :: byn(*) 
      real(double) , intent(in) :: bzn(*) 
      real(double) , intent(out) :: bxv(*) 
      real(double) , intent(out) :: byv(*) 
      real(double) , intent(out) :: bzv(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: k, j, i, ijk 
      real(double) :: rvvol 
!-----------------------------------------------
!
      do k = k1, k2 
         do j = j1, j2 
            do i = i1, i2 
!
               ijk = (k - 1)*kwid + (j - 1)*jwid + (i - 1)*iwid + 1 
!
               rvvol = 1./(vol(ijk)+vol(ijk-iwid)+vol(ijk-iwid-jwid)+vol(ijk-&
                  jwid)+vol(ijk-kwid)+vol(ijk-iwid-kwid)+vol(ijk-iwid-jwid-kwid&
                  )+vol(ijk-jwid-kwid)) 
!
               bxv(ijk) = (bxn(ijk)*vol(ijk)+bxn(ijk-iwid)*vol(ijk-iwid)+bxn(&
                  ijk-iwid-jwid)*vol(ijk-iwid-jwid)+bxn(ijk-jwid)*vol(ijk-jwid)&
                  +bxn(ijk-kwid)*vol(ijk-kwid)+bxn(ijk-iwid-kwid)*vol(ijk-iwid-&
                  kwid)+bxn(ijk-iwid-jwid-kwid)*vol(ijk-iwid-jwid-kwid)+bxn(ijk&
                  -jwid-kwid)*vol(ijk-jwid-kwid))*rvvol 
!
               byv(ijk) = (byn(ijk)*vol(ijk)+byn(ijk-iwid)*vol(ijk-iwid)+byn(&
                  ijk-iwid-jwid)*vol(ijk-iwid-jwid)+byn(ijk-jwid)*vol(ijk-jwid)&
                  +byn(ijk-kwid)*vol(ijk-kwid)+byn(ijk-iwid-kwid)*vol(ijk-iwid-&
                  kwid)+byn(ijk-iwid-jwid-kwid)*vol(ijk-iwid-jwid-kwid)+byn(ijk&
                  -jwid-kwid)*vol(ijk-jwid-kwid))*rvvol 
!
               bzv(ijk) = (bzn(ijk)*vol(ijk)+bzn(ijk-iwid)*vol(ijk-iwid)+bzn(&
                  ijk-iwid-jwid)*vol(ijk-iwid-jwid)+bzn(ijk-jwid)*vol(ijk-jwid)&
                  +bzn(ijk-kwid)*vol(ijk-kwid)+bzn(ijk-iwid-kwid)*vol(ijk-iwid-&
                  kwid)+bzn(ijk-iwid-jwid-kwid)*vol(ijk-iwid-jwid-kwid)+bzn(ijk&
                  -jwid-kwid)*vol(ijk-jwid-kwid))*rvvol 
!
            end do 
         end do 
      end do 
!
      return  
      end subroutine vtxb 
