      subroutine volume_vtx(ncells, ijkcell, iwid, jwid, kwid, x, y, z, c1x, &
         c2x, c3x, c4x, c5x, c6x, c7x, c8x, c1y, c2y, c3y, c4y, c5y, c6y, c7y, &
         c8y, c1z, c2z, c3z, c4z, c5z, c6z, c7z, c8z, vvol) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: ncells 
      integer , intent(in) :: iwid 
      integer , intent(in) :: jwid 
      integer , intent(in) :: kwid 
      integer , intent(in) :: ijkcell(*) 
      real(double) , intent(in) :: x(*) 
      real(double)  :: y(*) 
      real(double)  :: z(*) 
      real(double) , intent(in) :: c1x(*) 
      real(double) , intent(in) :: c2x(*) 
      real(double) , intent(in) :: c3x(*) 
      real(double) , intent(in) :: c4x(*) 
      real(double) , intent(in) :: c5x(*) 
      real(double) , intent(in) :: c6x(*) 
      real(double) , intent(in) :: c7x(*) 
      real(double) , intent(in) :: c8x(*) 
      real(double)  :: c1y(*) 
      real(double)  :: c2y(*) 
      real(double)  :: c3y(*) 
      real(double)  :: c4y(*) 
      real(double)  :: c5y(*) 
      real(double)  :: c6y(*) 
      real(double)  :: c7y(*) 
      real(double)  :: c8y(*) 
      real(double)  :: c1z(*) 
      real(double)  :: c2z(*) 
      real(double)  :: c3z(*) 
      real(double)  :: c4z(*) 
      real(double)  :: c5z(*) 
      real(double)  :: c6z(*) 
      real(double)  :: c7z(*) 
      real(double)  :: c8z(*) 
      real(double) , intent(inout) :: vvol(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: n, ijk 
      real(double) :: xc 
!-----------------------------------------------
!
      do n = 1, ncells 
         ijk = ijkcell(n) 
!
         xc = 0.125*(x(ijk+iwid)+x(ijk+iwid+jwid)+x(ijk+jwid)+x(ijk)+x(ijk+iwid&
            +kwid)+x(ijk+iwid+jwid+kwid)+x(ijk+jwid+kwid)+x(ijk+kwid)) 
!
         vvol(ijk+iwid) = vvol(ijk+iwid) - c1x(ijk)*xc 
!
         vvol(ijk+iwid+jwid) = vvol(ijk+iwid+jwid) - c2x(ijk)*xc 
!
         vvol(ijk+jwid) = vvol(ijk+jwid) - c3x(ijk)*xc 
!
         vvol(ijk) = vvol(ijk) - c4x(ijk)*xc 
!
         vvol(ijk+iwid+kwid) = vvol(ijk+iwid+kwid) - c5x(ijk)*xc 
!
         vvol(ijk+iwid+jwid+kwid) = vvol(ijk+iwid+jwid+kwid) - c6x(ijk)*xc 
!
         vvol(ijk+jwid+kwid) = vvol(ijk+jwid+kwid) - c7x(ijk)*xc 
!
         vvol(ijk+kwid) = vvol(ijk+kwid) - c8x(ijk)*xc 
!
      end do 
!
      return  
      end subroutine volume_vtx 
