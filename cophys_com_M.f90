      module cophys_com_M 
      use vast_kind_param, only:  double 
!...Created by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
      integer :: idvb, jdvb, kdvb 
      real(double), dimension(1) :: com5 
      real(double), dimension(400) :: rwz 
      real(double) :: gm1, rho0, beta, asq, a, wsa, h, tke, tie, etotl, tmass, &
         tmomx, tmomy, tmomz, tmom, bl0, bl1, bl2, bl3, bl23, bvrt, bl0sv, &
         bl1sv, bl2sv, bl3sv, bvrtsv, tbe, tle, dx, dy, dz, rpl, rvac, rwall, &
         rmaj, cdlhf, sdlhf, wghti, wghtj, elpp, elpt, eltt, zdivb, cdlt, sdlt&
         , cdph, sdph, cdphhf, sdphhf, dphi, tmax, tmax0, tmax1, tmax2, tmax3, &
         rdl0sv, rdl1sv, del2sv, del0, del1, del2, del3, del4, del5, del7, &
         bl23sv, rq0, btorus, bvertcl, rhoi, siei, racc, zacc, tflux, pflux, gx&
         , gy, gz, tiesav, tkesav, tbesav, dvbmax, angv, resist, tangv, omgdt, &
         q0, coll, efld0, c5last, el_rec, bx_ini, by_ini, eps_rec,&
         nu_len, epsil, ex_ini, ey_ini, ez_ini,&
         rnu_rec, bz_ini, ex_ext, ey_ext, ez_ext, ex_const, ey_const, ez_const
      logical :: fldivb, cartesian, nu_comm 
      end module cophys_com_M 
