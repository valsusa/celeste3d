      subroutine modefft2d(ibp2, jbp2, kbp2, bzv, iwid, jwid, kwid, idx, idy, &
         idz, rmod, imod, fftx, ffty, fftz, fftu, fftv, fftw) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double, dble_complex 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: ibp2 
      integer , intent(in) :: jbp2 
      integer , intent(in) :: kbp2 
      integer , intent(in) :: iwid 
      integer , intent(in) :: jwid 
      integer , intent(in) :: kwid 
      integer , intent(in) :: idx 
      integer , intent(in) :: idy 
      integer , intent(in) :: idz 
      real(double) , intent(in) :: bzv(*) 
      real(double) , intent(out) :: rmod(20,20) 
      real(double) , intent(out) :: imod(20,20) 
      real(double) , intent(out) :: fftx(*) 
      real(double) , intent(out) :: ffty(*) 
      real(double) , intent(out) :: fftz(*) 
      real(double) , intent(out) :: fftu(*) 
      real(double) , intent(out) :: fftv(*) 
      real(double) , intent(out) :: fftw(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i, j, ibp1, jbp1, kbp1, n, kmed, kstart, kend, k, ijk 
      character :: zname*8, name*80 
      complex(dble_complex), dimension(idx + 2,idy + 2) :: ffta, fftc 
!-----------------------------------------------
 
      imod = 0. 
      rmod = 0. 
 
      ibp1 = ibp2 - 1 
      jbp1 = jbp2 - 1 
      kbp1 = kbp2 - 1 
 
      fftx(:idx*idy*idz) = 0. 
      ffty(:idx*idy*idz) = 0. 
      fftz(:idx*idy*idz) = 0. 
      fftu(:idx*idy*idz) = 0. 
      fftv(:idx*idy*idz) = 0. 
      fftw(:idx*idy*idz) = 0. 
 
      kmed = 2 + (kbp2 - 2 + 1)/2 
!      kstart=kmed
!      kend=kmed
      kstart = 2 
      kend = kbp2 
!
!     Ho provato +-1 per fare 3 paini, risultati quasi identici a cosi`
!
      do i = 2, ibp2 
         do j = 2, jbp2 
            ffta(i-1,j-1) = 0. 
            ffta(i-1,j-1) = ffta(i-1,j-1) + sum(bzv(kwid*(kstart-1)+1+(i-1)*&
               iwid+(j-1)*jwid:(kend-1)*kwid+1+(i-1)*iwid+(j-1)*jwid:kwid)) 
            ffta(i-1,j-1) = ffta(i-1,j-1)/float(kend - kstart + 1) 
!      ffta(i-1,j-1)=sin(3*2*3.14*(i-2)/float(ibp2-2))
         end do 
      end do 
 
      call fft2d (ibp1, jbp1, ffta, idx + 2, fftc, idx + 2) 
!
      do i = 1, min(20,idx + 2) 
         do j = 1, min(20,idy + 2) 
            rmod(i,j) = dble(fftc(i,j)) 
            imod(i,j) = aimag(fftc(i,j)) 
         end do 
      end do 
!
      return  
      end subroutine modefft2d 
