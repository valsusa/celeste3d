      subroutine gdone
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02
!...Switches: -yf -x1
      implicit none
!-----------------------------------------------
      return
      end subroutine gdone


      subroutine getjtl(itlim)
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02
!...Switches: -yf -x1
      USE vast_kind_param, ONLY:  double
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      real(double) , intent(out) :: itlim
!-----------------------------------------------
!
!     set maximum running time
!
      itlim = 8600000d0
      return
      end subroutine getjtl



