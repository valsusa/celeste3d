      module corgan_com_M
      use vast_kind_param, only:  double
!...Created by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02
      real(double), parameter :: clite = 1.
      real(double), parameter :: fourpi = 1.
      integer, parameter :: nhst = 5000
      integer, parameter :: ijmx = 1500
      integer, parameter :: nsampl = 1024
      integer, parameter :: lay = 1
      integer, parameter :: nreg = 4
      integer, parameter :: nsp = 2
      integer, parameter :: idx = 128
      integer, parameter :: idy = 1
      integer, parameter :: idz = 128
      integer, parameter :: idxg = idx + 2
      integer, parameter :: idyg = idy + 2
      integer, parameter :: idzg = idz + 2
      integer, parameter :: itdim = idxg*idyg*idzg*lay
      integer, parameter :: idxyzg = idxg*idyg*idzg
      integer, parameter :: npart =4000000
      integer, parameter :: nlist = 150
      integer, parameter :: npart_obj = 8000
      integer, parameter :: nreg_obj = 2
      integer, dimension(300) :: iplb1, iplb2
      integer, dimension(1024) :: ipbuff
      real(double), dimension(ijmx) :: cq
      integer, dimension(4) :: idisp
      integer :: krd, kpr, kpt, kfm, kpl, ktpin, ktpout, jbnm, numtd, &
         itlwd, mgeom, ncr1, ncr2, ncr3, ixto, lpr, itrlmp, ncyc, ncyc1, ncyc2&
         , nmeflm, nflm
      real(double), dimension(1) :: com1
      real(double), dimension(10) :: dto, dtoc, param
      real(double) :: tlm, stim, t1, t2, dcttd, dpttd, pttd, tout, tmovi&
         , pflag, pars, strait, toroid, tlimd, twfin, tt, c1last, cntr, avg&
         , itlm
      logical :: wrtp, outflg, precon, eandm, anydrift, shape, touch, frctn, &
         periodic, collide, newstuf
      character(len=80) :: nome_file_input, nome_run
      character(len=8) :: ddate
      character(len=10) :: hour
      end module corgan_com_M
