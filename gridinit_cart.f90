     subroutine gridinit_cart(ibar, jbar, kbar, iwid, jwid, kwid, dx, dy, dz, &
         x, y, z, xl, xr, yb, yt, ze, zf, nu_len, nu_comm) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: ibar 
      integer , intent(in) :: jbar 
      integer , intent(in) :: kbar 
      integer , intent(in) :: iwid 
      integer , intent(in) :: jwid 
      integer , intent(in) :: kwid 
      real(double) , intent(in) :: dx 
      real(double) , intent(in) :: dy 
      real(double) , intent(in) :: dz 
      real(double) , intent(out) :: xl 
      real(double) , intent(out) :: xr 
      real(double) , intent(out) :: yb 
      real(double) , intent(out) :: yt 
      real(double) , intent(out) :: ze 
      real(double) , intent(out) :: zf 
      real(double) , intent(in) :: nu_len 
      logical , intent(in) :: nu_comm 
      real(double) , intent(out) :: x(*) 
      real(double) , intent(out) :: y(*) 
      real(double) , intent(out) :: z(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: n, ic, k, j, i, ijk 
      real(double) :: r, diz 
      logical :: cartesian 
!-----------------------------------------------
 
      r = kbar*dz 
      n = kbar + 1 
      ic = (n - 1)/2 + 2 
      diz = n/nu_len 
!        write(77,*)R,N,ic,diz
 
      if (.not.nu_comm) then 
         do k = 1, kbar + 2 
            do j = 1, jbar + 2 
               do i = 1, ibar + 2 
 
                  ijk = (k - 1)*kwid + (j - 1)*jwid + (i - 1)*iwid + 1 
 
                  x(ijk) = (i - 2)*dx 
                  y(ijk) = (j - 2)*dy 
!
!     uniform grid
!
                  z(ijk) = (k - 2)*dz 
!        write(77,*)k,z(ijk)
               end do 
            end do 
         end do 
      else 
         do k = 1, kbar + 2 
!        write(77,*)k,z(ijk)
            do j = 1, jbar + 2 
!        write(77,*)k,z(ijk)
               do i = 1, ibar + 2 
 
                  ijk = (k - 1)*kwid + (j - 1)*jwid + (i - 1)*iwid + 1 
 
                  x(ijk) = (i - 2)*dx 
                  y(ijk) = (j - 2)*dy 
!
!     non-uniform grid in z
!
                  z(ijk) = r*(sinh((k - ic)/diz) - sinh((2 - ic)/diz))/(sinh((n&
                      + 1 - ic)/diz) - sinh((2 - ic)/diz)) 
!        write(77,*)k,z(ijk)
               end do 
            end do 
         end do 
      endif 
 
      xl = 0. 
      xr = float(ibar)*dx 
      yb = 0. 
      yt = float(jbar)*dy 
      ze = 0. 
      zf = float(kbar)*dz 
 
      return  
      end subroutine gridinit_cart 
