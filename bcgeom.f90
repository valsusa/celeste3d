      subroutine ghostlist(mwid, m, lwid, l1, l2, nwid, n1, n2, nsurf, ijkvtmp) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: mwid 
      integer , intent(in) :: m 
      integer , intent(in) :: lwid 
      integer , intent(in) :: l1 
      integer , intent(in) :: l2 
      integer , intent(in) :: nwid 
      integer , intent(in) :: n1 
      integer , intent(in) :: n2 
      integer , intent(out) :: nsurf 
      integer , intent(out) :: ijkvtmp(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: l, n, ijk 
!-----------------------------------------------
!
!     subroutine to list the ghost vertices attached to a boundary
!     l,n indices on the surface
!     m1 specifies what is the level of the surface
!
      nsurf = 0 
      do l = l1, l2 
         do n = n1, n2 
            ijk = 1 + (l - 1)*lwid + (n - 1)*nwid + (m - 1)*mwid 
            nsurf = nsurf + 1 
            ijkvtmp(nsurf) = ijk 
         end do 
      end do 
 
      return  
      end subroutine ghostlist 


 
 
      subroutine ghostnorm(nsurf, ijkvtmp, vnormx, vnormy, vnormz, avnormx, &
         avnormy, avnormz) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nsurf 
      real(double) , intent(in) :: avnormx 
      real(double) , intent(in) :: avnormy 
      real(double) , intent(in) :: avnormz 
      integer , intent(in) :: ijkvtmp(*) 
      real(double) , intent(out) :: vnormx(*) 
      real(double) , intent(out) :: vnormy(*) 
      real(double) , intent(out) :: vnormz(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: n, ijk 
!-----------------------------------------------
!
!     subroutine to evaluate the norm at a ghost vertex
!     at the moment it assumes plane surfaces
!
      vnormx(:nsurf) = avnormx 
      vnormy(:nsurf) = avnormy 
      vnormz(:nsurf) = avnormz 
 
      return  
      end subroutine ghostnorm 


 
 
      subroutine ghostgeom(nsurf, ijkvtmp, vnormx, vnormy, vnormz, gcx, gcy, &
         gcz, ghgcx, ghgcy, ghgcz, nskip) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nsurf 
      integer , intent(in) :: nskip 
      integer , intent(in) :: ijkvtmp(*) 
      real(double) , intent(in) :: vnormx(*) 
      real(double) , intent(in) :: vnormy(*) 
      real(double) , intent(in) :: vnormz(*) 
      real(double) , intent(in) :: gcx(*) 
      real(double) , intent(in) :: gcy(*) 
      real(double) , intent(in) :: gcz(*) 
      real(double) , intent(out) :: ghgcx(*) 
      real(double) , intent(out) :: ghgcy(*) 
      real(double) , intent(out) :: ghgcz(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: n, ijkgh, ijk 
      real(double) :: scal 
!-----------------------------------------------
!
!     a subroutine to create ghost geometric coefficient out
!     of black magic.......... Abracadabra.
!
      do n = 1, nsurf 
         ijkgh = ijkvtmp(n) 
         ijk = ijkgh + nskip 
         scal = gcx(ijk)*vnormx(n) + gcy(ijk)*vnormy(n) + gcz(ijk)*vnormz(n) 
         ghgcx(ijkgh) = gcx(ijk) - 2.*vnormx(n)*scal 
         ghgcy(ijkgh) = gcy(ijk) - 2.*vnormy(n)*scal 
         ghgcz(ijkgh) = gcz(ijk) - 2.*vnormz(n)*scal 
      end do 
 
      return  
      end subroutine ghostgeom 
