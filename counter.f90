      subroutine counter(ncells, ijkcell, iphead, link, number, npart, where) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: ncells 
      integer , intent(in) :: npart 
      real(double)  :: where 
      integer , intent(in) :: ijkcell(*) 
      integer , intent(in) :: iphead(*) 
      integer , intent(in) :: link(0:npart) 
      real(double) , intent(inout) :: number(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: n, ijk, np, ijkmin, ijkmax, l 
      real(double), dimension(10) :: bin 
      real(double) :: minnum, maxnum, totnum, dnum, avgnsq, avgn, variance, &
         stddev 
!-----------------------------------------------
!
!
      do n = 1, ncells 
!
         ijk = ijkcell(n) 
!
         number(ijk) = 0.0 
!
         if (iphead(ijk) == 0) cycle  
!
         np = iphead(ijk) 
!
         number(ijk) = number(ijk) + 1. 
         np = link(np) 
         do while(np /= 0) 
            number(ijk) = number(ijk) + 1. 
            np = link(np) 
         end do 
!
      end do 
!
      totnum = 0.0 
      minnum = 1.E10 
      maxnum = 0.0 
!
      do n = 1, ncells 
!
         ijk = ijkcell(n) 
         totnum = totnum + number(ijk) 
!
         if (minnum > number(ijk)) then 
!
            ijkmin = ijk 
            minnum = number(ijk) 
         endif 
!
         if (maxnum >= number(ijk)) cycle  
         ijkmax = ijk 
         maxnum = number(ijk) 
!
      end do 
!
      write (6, *) 'TOTAL NO PART:', totnum 
!
!     analysis
!
      dnum = (maxnum - minnum)/10. 
!
      bin = 0.0 
!
      if (dnum > 0.) then 
         do n = 1, ncells 
            ijk = ijkcell(n) 
            l = int((number(ijk)-minnum)/dnum) 
            bin(l) = bin(l) + 1. 
         end do 
!
      endif 
!
      avgnsq = sum(number(ijkcell(:ncells))**2) 
      avgn = sum(number(ijkcell(:ncells))) 
!
      avgnsq = avgnsq/float(ncells) 
      avgn = avgn/float(ncells) 
!
      variance = avgnsq - avgn**2 
      stddev = sqrt(variance) 
!
      return  
      end subroutine counter 
