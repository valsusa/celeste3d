!dk triquad
      subroutine triquad(ijkc, bx, by, bz, w, bxp, byp, bzp) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      real(double) , intent(out) :: bxp 
      real(double) , intent(out) :: byp 
      real(double) , intent(out) :: bzp 
      integer , intent(in) :: ijkc(27) 
      real(double) , intent(in) :: bx(*) 
      real(double) , intent(in) :: by(*) 
      real(double) , intent(in) :: bz(*) 
      real(double) , intent(in) :: w(27) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
!-----------------------------------------------
!
      bxp = w(1)*bx(ijkc(1)) + (w(2)*bx(ijkc(2))+(w(3)*bx(ijkc(3))+(w(4)*bx(&
         ijkc(4))+(w(5)*bx(ijkc(5))+(w(6)*bx(ijkc(6))+(w(7)*bx(ijkc(7))+(w(8)*&
         bx(ijkc(8))+w(9)*bx(ijkc(9))))))))) 
      bxp = bxp + (w(10)*bx(ijkc(10))+(w(11)*bx(ijkc(11))+(w(12)*bx(ijkc(12))+(&
         w(13)*bx(ijkc(13))+(w(14)*bx(ijkc(14))+(w(15)*bx(ijkc(15))+(w(16)*bx(&
         ijkc(16))+(w(17)*bx(ijkc(17))+w(18)*bx(ijkc(18)))))))))) 
      bxp = bxp + (w(19)*bx(ijkc(19))+(w(20)*bx(ijkc(20))+(w(21)*bx(ijkc(21))+(&
         w(22)*bx(ijkc(22))+(w(23)*bx(ijkc(23))+(w(24)*bx(ijkc(24))+(w(25)*bx(&
         ijkc(25))+(w(26)*bx(ijkc(26))+w(27)*bx(ijkc(27)))))))))) 
!
      byp = w(1)*by(ijkc(1)) + (w(2)*by(ijkc(2))+(w(3)*by(ijkc(3))+(w(4)*by(&
         ijkc(4))+(w(5)*by(ijkc(5))+(w(6)*by(ijkc(6))+(w(7)*by(ijkc(7))+(w(8)*&
         by(ijkc(8))+w(9)*by(ijkc(9))))))))) 
      byp = byp + (w(10)*by(ijkc(10))+(w(11)*by(ijkc(11))+(w(12)*by(ijkc(12))+(&
         w(13)*by(ijkc(13))+(w(14)*by(ijkc(14))+(w(15)*by(ijkc(15))+(w(16)*by(&
         ijkc(16))+(w(17)*by(ijkc(17))+w(18)*by(ijkc(18)))))))))) 
      byp = byp + (w(19)*by(ijkc(19))+(w(20)*by(ijkc(20))+(w(21)*by(ijkc(21))+(&
         w(22)*by(ijkc(22))+(w(23)*by(ijkc(23))+(w(24)*by(ijkc(24))+(w(25)*by(&
         ijkc(25))+(w(26)*by(ijkc(26))+w(27)*by(ijkc(27)))))))))) 
!
      bzp = w(1)*bz(ijkc(1)) + (w(2)*bz(ijkc(2))+(w(3)*bz(ijkc(3))+(w(4)*bz(&
         ijkc(4))+(w(5)*bz(ijkc(5))+(w(6)*bz(ijkc(6))+(w(7)*bz(ijkc(7))+(w(8)*&
         bz(ijkc(8))+w(9)*bz(ijkc(9))))))))) 
      bzp = bzp + (w(10)*bz(ijkc(10))+(w(11)*bz(ijkc(11))+(w(12)*bz(ijkc(12))+(&
         w(13)*bz(ijkc(13))+(w(14)*bz(ijkc(14))+(w(15)*bz(ijkc(15))+(w(16)*bz(&
         ijkc(16))+(w(17)*bz(ijkc(17))+w(18)*bz(ijkc(18)))))))))) 
      bzp = bzp + (w(19)*bz(ijkc(19))+(w(20)*bz(ijkc(20))+(w(21)*bz(ijkc(21))+(&
         w(22)*bz(ijkc(22))+(w(23)*bz(ijkc(23))+(w(24)*bz(ijkc(24))+(w(25)*bz(&
         ijkc(25))+(w(26)*bz(ijkc(26))+w(27)*bz(ijkc(27)))))))))) 
!
      return  
      end subroutine triquad 
