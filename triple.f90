      subroutine triple(x, y, z, ijk, iwid, jwid, kwid, vol) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: ijk 
      integer , intent(in) :: iwid 
      integer , intent(in) :: jwid 
      integer , intent(in) :: kwid 
      real(double) , intent(out) :: vol 
      real(double) , intent(in) :: x(*) 
      real(double) , intent(in) :: y(*) 
      real(double) , intent(in) :: z(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
!-----------------------------------------------
!
      vol = ((x(ijk+iwid)-x(ijk))*(y(ijk+jwid)-y(ijk))-(y(ijk+iwid)-y(ijk))*(x(&
         ijk+jwid)-x(ijk)))*(z(ijk+kwid)-z(ijk)) + ((y(ijk+iwid)-y(ijk))*(z(ijk&
         +jwid)-z(ijk))-(z(ijk+iwid)-z(ijk))*(y(ijk+jwid)-y(ijk)))*(x(ijk+kwid)&
         -x(ijk)) + ((z(ijk+iwid)-z(ijk))*(x(ijk+jwid)-x(ijk))-(x(ijk+iwid)-x(&
         ijk))*(z(ijk+jwid)-z(ijk)))*(y(ijk+kwid)-y(ijk)) 
!
      return  
      end subroutine triple 
