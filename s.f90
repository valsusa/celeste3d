!
!
!
!
!
      real(kind(0.0d0)) function s (xx) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use corgan_com_M 
      use cindex_com_M 
      use numpar_com_M 
      use cprplt_com_M, ONLY:  
      use cophys_com_M 
      use ctemp_com_M, ONLY: fmf, fef, fvf, fbxf, fbyf, fbzf, twx 
      use blcom_com_M, ONLY:  
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      real(double) , intent(inout) :: xx(*) 
!-----------------------------------------------
!   L o c a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer , dimension(8) :: iv 
      integer, dimension(4) :: lioinput, lioprint 
      real(double), dimension(1) :: ixi1, ixi2, ieta1, ieta2 
      real(double) :: ixi4, ieta4, ixi5, ieta5 
      real(double), dimension(8) :: wght 
      real(double), dimension(100) :: fpxf, fpyf, fpzf, fpxft, fpyft, fpzft, &
         ucm, vcm, wcm, cm 
      real(double), dimension(3,20) :: wsin, wcos 
      real(double), dimension(3,12,20) :: tsi, rot 
      real(double) :: wsx, wsy, wsz, hz, snhz, cshz, snhz2, cshz2, wsr, hlsft, &
         ecc, wsb, wsxp, wsyp, wsxpp, wsypp 
      logical :: expnd, nomore 
      character :: name*80 
!-----------------------------------------------
!
!l    -------------------------------------------------------
!
!ll   general formalism for both potentials and shaped coil:
      wsx = strait*xx(1) + toroid*sqrt(xx(1)**2+xx(2)**2) - rmaj 
      wsy = xx(3) 
      if (xx(1) == 0.) xx(1) = 1.E-20 
      wsz = strait*xx(2) + rmaj*atan2(xx(2),xx(1)) 
      hz = h*wsz 
      snhz = sin(hz) 
      cshz = cos(hz) 
      snhz2 = sin(0.5*hz) 
      cshz2 = cos(0.5*hz) 
      wsr = (-del0*cshz) + rwz(j) 
      hlsft = del1 
      ecc = del2 
      wsa = 1. - ecc 
      wsb = 1. + ecc 
      wsxp = wsx - hlsft*cshz 
      wsyp = wsy - hlsft*snhz 
      wsxpp = cshz2*wsxp + snhz2*wsyp 
      wsypp = (-snhz2*wsxp) + cshz2*wsyp 
      s = (wsxpp/wsa)**2 + (wsypp/wsb)**2 - wsr**2 
      return  
!
      end function s 
