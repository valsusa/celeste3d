      real(kind(0.0d0)) function sdot (n, x, incx, y, incy)
!-----------------------------------------------
!   M o d u l e s
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double
      use modify_com_M

!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02
!...Switches: -yf -x1
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: n
      integer , intent(in) :: incx
      integer , intent(in) :: incy
      real(double) , intent(in) :: x(*)
      real(double) , intent(in) :: y(*)
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i
!-----------------------------------------------
!     eorks only if incx,incy=1
      if (incx/=1 .or. incy/=1) write (*, *) 'sdot fasilure'
      sdot = dot_product(x(:n),y(:n))
      return
      end function sdot



      real(kind(0.0d0)) function snrm2 (n, x, inc)
!-----------------------------------------------
!   M o d u l e s
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double
      use modify_com_M

!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02
!...Switches: -yf -x1
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: n
      integer , intent(in) :: inc
      real(double) , intent(in) :: x(*)
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i
!-----------------------------------------------
!     works only if inc=1
      if (inc /= 1) write (*, *) 'snrm2 failure'
      snrm2 = sum(x(:n)**2)
      snrm2 = sqrt(snrm2)
      return
      end function snrm2



      real(kind(0.0d0)) function sasum (n, x, inc)
!-----------------------------------------------
!   M o d u l e s
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double
      use modify_com_M

!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02
!...Switches: -yf -x1
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: n
      integer , intent(in) :: inc
      real(double) , intent(in) :: x(*)
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i
!-----------------------------------------------
!     works only if inc=1
      if (inc /= 1) write (*, *) 'sasum failure'
      sasum = 0.
      do i = 1, n
         sasum = sasum + abs(x(i))
      end do
      return
      end function sasum



      integer function ismax (n, x, inc)
!-----------------------------------------------
!   M o d u l e s
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double
      use modify_com_M

!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02
!...Switches: -yf -x1
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: n
      integer , intent(in) :: inc
      real(double) , intent(in) :: x(*)
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i
!-----------------------------------------------
!     works only if inc=1
      if (inc /= 1) write (*, *) 'ismax failure'
      ismax = 1
      do i = 2, n
         if (x(i) <= x(ismax)) cycle
         ismax = i
      end do
      return
      end function ismax



      integer function ismin (n, x, inc)
!-----------------------------------------------
!   M o d u l e s
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double
      use modify_com_M

!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02
!...Switches: -yf -x1
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: n
      integer , intent(in) :: inc
      real(double) , intent(in) :: x(*)
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i
!-----------------------------------------------
!     works only if inc=1
      if (inc /= 1) write (*, *) 'ismin failure'
      ismin = 1
      do i = 2, n
         if (x(i) >= x(ismin)) cycle
         ismin = i
      end do
      return
      end function ismin



      subroutine second(t)
!-----------------------------------------------
!   M o d u l e s
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      real(double)  :: t
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
!-----------------------------------------------
      t=0d0
      return
      end subroutine second
      function ranf()
      real(8) :: ranf
      real(4) :: harvest
!       HP
!     ranf=drand48()
!       LAHEY fortran - LINUX
      call random_number(harvest)
      ranf=dble(harvest)
      return
      end



      integer function cvmgp (i, j, x)
!-----------------------------------------------
!   M o d u l e s
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02
!...Switches: -yf -x1
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: i
      integer , intent(in) :: j
      real(double) , intent(in) :: x
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
!-----------------------------------------------
      cvmgp = j
      if (x >= 0.D0) cvmgp = i
      return
      end function cvmgp



      real(kind(0.0d0)) function ccvmgz (i, j, x)
!-----------------------------------------------
!   M o d u l e s
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02
!...Switches: -yf -x1
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: x
      real(double) , intent(in) :: i
      real(double) , intent(in) :: j
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
!-----------------------------------------------
      ccvmgz = j
      if (x == 0) ccvmgz = i
      return
      end function ccvmgz
