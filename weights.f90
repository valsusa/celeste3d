!dk weights
      subroutine weights(xi, eta, zeta, wght) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      real(double) , intent(in) :: xi 
      real(double) , intent(in) :: eta 
      real(double) , intent(in) :: zeta 
      real(double) , intent(out) :: wght(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      real(double) :: omx, ome, omz 
!-----------------------------------------------
!
      omx = 1. - xi 
      ome = 1. - eta 
      omz = 1. - zeta 
!
      wght(1) = xi*omz*ome 
      wght(2) = xi*omz*eta 
      wght(3) = omx*omz*eta 
      wght(4) = omx*omz*ome 
      wght(5) = xi*zeta*ome 
      wght(6) = xi*zeta*eta 
      wght(7) = omx*zeta*eta 
      wght(8) = omx*zeta*ome 
!
      return  
      end subroutine weights 
