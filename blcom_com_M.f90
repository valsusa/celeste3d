      module blcom_com_M
      use vast_kind_param, only:  double
!...Created by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02
      use corgan_com_M, only: nreg
      use corgan_com_M, only: nsp
      use corgan_com_M, only: idx
      use corgan_com_M, only: idxg
      use corgan_com_M, only: idy, idyg, idz, idzg, lay, itdim, nsp
      use corgan_com_M, only: nhst
      use corgan_com_M, only: npart
      use corgan_com_M, only: nlist
      use corgan_com_M, only: nsampl
      integer, dimension(nreg) :: npcelx, npcely, npcelz, icoi
      integer, dimension(0:nreg) :: numtot
      integer, dimension(nsp) :: npcel_control
      integer :: mpert, mtresh, iplost, nplost
      real(double), dimension(8,nreg) :: xvi, yvi, zvi
      real(double), dimension(nreg) :: uvi, vvi, wvi, utordrft, uvip, vvip, &
         wvip, siepx, siepy, siepz, rhr, qom, t_wall, wsr_itg, el_itg
      logical, dimension(nreg) ::   inject_end,inject_front,inject_left,inject_right
      real(double) :: pert_gem, pert_x, vtresh, qlost, plost_pos, plost_neg, wave_amplitude
      logical, dimension(nreg) :: drift
      logical :: shear, lsplit, lcoal, forcefree, shock, fluid_load, external_current
      logical :: divergence_cleaning
      logical :: relativity
      integer :: initial_equilibrium
      integer, dimension(itdim) :: iphd2
      real(double), dimension(itdim,29) :: wate
      real(double), dimension(itdim,nsp) :: number
      real(double), dimension(itdim) :: sie, rho, rhol
      real(double), dimension(itdim) :: qdnc
      real(double), dimension(itdim,nsp) :: qdnv
      real(double), dimension(itdim) :: mask, jx0, jy0, jz0, jxtilde, jytilde, &
         jztilde, jxtildet, jytildet, jztildet, pixx, pixy, pixz, piyy, piyz, &
         pizz, pixysp2, pixzsp2, piyzsp2, pixxsp2,piyysp2,pizzsp2,  &
         pixysp1, pixzsp1, piyzsp1, pixxsp1,piyysp1,pizzsp1, &
		 uxsp1,uysp1,uzsp1,densp1,uxsp2,uysp2,uzsp2,densp2, &
         gradxchi, gradychi, gradzchi, divpix, divpiy, &
         divpiz, colx, coly, colz, jmag, j12x, j12y, j12z, joldx, joldy, joldz, &
         jx0old, jy0old, jz0old
      real(double), dimension(itdim,nsp) :: jxs, jys, jzs
      real(double), dimension(itdim) :: decyk, jdote, qdnc0
      real(double), dimension(itdim) :: udrft, vdrft, wdrft, bxn, byn, bzn, bxv&
         , byv, bzv, p, gradxb, gradyb, ay, gradzb, absb
      real(double), dimension(idzg) :: iota
      real(double), dimension(itdim) :: psi
      real(double) :: delta
      integer :: itmax, itsub
      real(double), dimension(itdim) :: a11, a12, a13, a21, a22, a23, a31, a32&
         , a33
      real(double), dimension(itdim) :: phipot, potavp, curlx, curly, curlz, ex0, ey0, &
         ez0, exold, eyold, ezold, ex, ey, ez,exavp,eyavp,ezavp,susxx&
         ,susxy,susxz,suszx,suszy,suszz
      real(double), dimension(0:nhst) :: efnrg, ebnrg, ekenrg, ekenrgx, ekenrgy, ekenrgz, &
         ekinrg, ekinrgx, ekinrgy, ekinrgz, efnrgx, &
         efnrgy, efnrgz, ebnrgx, ebnrgy, ebnrgz, eoenrg, eoinrg, charge, &
         thistry, toroidal_j, citer_maxwell, citer_poisson, costcc, chcons1, &
         chcons2
      real(double) :: chnorm1, chnorm2
      integer, dimension(itdim) :: iphead, ijkcell, ijkvtx, ijktmp2, ijktmp3, &
         ijktmp4, ijkctmp
      integer, dimension(idzg) :: istep
      real(double), dimension(itdim) :: x, y, z, vol, vvol, vvol_half, area_x, &
         area_y, area_z, tsix, tsiy, tsiz, nux, nuy, nuz, etax, etay, etaz
      real(double) :: lama, lamb, lamc
      logical :: adaptg, fields, explicit
      real(double), dimension(itdim) :: c1x, c2x, c3x, c4x, c5x, c6x, c7x, c8x&
         , rxv, c1y, c2y, c3y, c4y, c5y, c6y, c7y, c8y, c1z, c2z, c3z, c4z, c5z&
         , c6z, c7z, c8z
      real(double), dimension(itdim) :: wgrid
      integer, dimension(0:npart) :: link, ico
      integer, dimension(nlist) :: npsav
      integer :: nptotl, npsampl
      real(double), dimension(0:npart) :: px, py, pz, pmu, pxi, peta, pzta, up&
         , vp, wp, qpar
      real(double), dimension(itdim) :: xptilde, yptilde, zptilde, uptilde, &
         vptilde, wptilde, bxpn, bypn, bzpn, expn, eypn, ezpn
      real(double), dimension(nsampl) :: vrms, proble
      integer, dimension(itdim) :: killer
      integer :: bcpl, bcpr, bcpt, bcpb, bcpe, bcpf, nfreq, nsmooth
      real(double) :: wkl, wkr, wke, wkf, xl, xr, yt, yb, ze, zf, &
         elle
      logical :: periodicx, plots, smoothing
      integer :: ncyc_restart
      logical :: restart
      end module blcom_com_M
