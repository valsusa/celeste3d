      module cprplt_com_M
      use vast_kind_param, only:  double
!...Created by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02
      use corgan_com_M, only: nhst
      real(double), dimension(0:nhst) :: xphys, yphys, zphys, upar, vpar, wpar, &
         bxpar, bypar, bzpar, mupar, epar
      end module cprplt_com_M
