      subroutine toroidal_coords(pi, rmaj, xp, yp, zp, phi, theta, r_minor) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      real(double) , intent(in) :: pi 
      real(double) , intent(in) :: rmaj 
      real(double) , intent(in) :: xp 
      real(double) , intent(in) :: yp 
      real(double) , intent(in) :: zp 
      real(double) , intent(out) :: phi 
      real(double) , intent(out) :: theta 
      real(double) , intent(out) :: r_minor 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      real(double) :: rxy 
!-----------------------------------------------
      phi = atan(yp/(xp + 1.E-8)) 
      if (xp < 0.0) phi = phi + pi 
!
      rxy = sqrt(xp**2 + yp**2) - rmaj 
      r_minor = sqrt(rxy**2 + zp**2) 
!
      theta = acos(zp/(r_minor + 1.E-8)) 
      if (rxy < 0.) theta = theta + pi 
!
      return  
      end subroutine toroidal_coords 
