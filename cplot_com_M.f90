      module cplot_com_M 
      use vast_kind_param, only:  double 
!...Created by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
      use corgan_com_M
      integer, dimension(25) :: iout 
      integer, dimension(nsp) :: idnum 
      integer :: iplot, cdfid, timdim, idtime, idcyc, idnx, idny, idnz, idu, &
         idv, idw, iddx, iddy, iddz, idrho, idp, idnumtot, idntot_o, idntot_r, &
         idcurlx, idcurly, idcurlz, iddiv, idsie, idpot, idjx, idjy, idjz, &
         idj0x, idj0y, idj0z, idbx, idby, idbz, iddens1, iddens2, idaymin, &
         idaymax, idrfsolar, iday, idex, idey, idez, idux1, iduy1, iduz1, idux2, iduy2, &
         iduz2, idpixysp2, idpiyzsp2, idpixxsp2, idpiyysp2,idpizzsp2, idpixzsp2, &
         idpixysp1, idpiyzsp1, idpixxsp1, idpiyysp1,idpizzsp1, idpixzsp1, &
         idwght, idfx, idfy, idfz, cdfidh, timdimh,&
         idtimeh, idtote, idtoti, idtotk, idtstr, idtelas, idmox, idmoy, &
         idmoz, idxcm, idycm, idzcm, iducm, idvcm, idwcm, idrotx, idroty, &
         idrotz, idrotrx, idrotry, idrotrz, idef, ideb, ideke, idekex, &
         idekey, idekez, ideki, idekix, idekiy, idekiz, ideoe, &
         ideoi, idchrg, idefx, idefy, idefz, idebx, ideby, idebz, ibxn0, ibxn1&
         , ibxn2, ibxn3, ibxn4, ibyn0, ibyn1, ibyn2, ibyn3, ibyn4, ibzn0, ibzn1&
         , ibzn2, ibzn3, ibzn4, iexn0, iexn1, iexn2, iexn3, iexn4, ieyn0, ieyn1&
         , ieyn2, ieyn3, ieyn4, iezn0, iezn1, iezn2, iezn3, iezn4, ijxn0, ijxn1&
         , ijxn2, ijxn3, ijxn4, ijyn0, ijyn1, ijyn2, ijyn3, ijyn4, ijzn0, ijzn1&
         , ijzn2, ijzn3, ijzn4, ibxm0, ibxm1, ibxm2, ibxm3, ibxm4, ibym0, ibym1&
         , ibym2, ibym3, ibym4, ibzm0, ibzm1, ibzm2, ibzm3, ibzm4, iexm0, iexm1&
         , iexm2, iexm3, iexm4, ieym0, ieym1, ieym2, ieym3, ieym4, iezm0, iezm1&
         , iezm2, iezm3, iezm4, ijxm0, ijxm1, ijxm2, ijxm3, ijxm4, ijym0, ijym1&
         , ijym2, ijym3, ijym4, ijzm0, ijzm1, ijzm2, ijzm3, ijzm4, idrmod, &
         idimod, idrmodz, idimodz, ichcons1, ichcons2, cdfidp, idtimep, idcycp&
         , timdimp, idnxp, idnyp, idnzp, idco, idup, idvp, idwp, idqp, idxp, &
         idyp, idzp 
      real(4) , dimension(npart) :: pxtmp, pytmp, pztmp, icotmp
      real(4) , dimension(20,20) :: pmod
      real(4) , dimension(itdim) :: plotx, ploty, plotz
      real(4) :: scalar
      end module cplot_com_M 
