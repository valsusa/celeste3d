	subroutine ParInject
        USE vast_kind_param
        use corgan_com_M 
        use cindex_com_M 
        use numpar_com_M 
        use cprplt_com_M
        use cophys_com_M 
        use ctemp_com_M
        use blcom_com_M
        use objects_com_M  
	implicit none
!
        integer :: n,ireg,is,np,inew,jnew,knew,ijknew,no_inj
        logical :: nomore
!	A subroutine to create and locate particles
!
        iphd2(ijkcell(:ncells))=0
!
        do ireg=1,nreg
           call rinj(ireg)
        enddo
!
        no_inj=0
    1   continue
        nomore=.true.
        ncellsp=0
        do n=1,ncells
  211      continue
           if(iphd2(ijkcell(n))<=0) cycle
           ijk=ijkcell(n)
           np=iphd2(ijk)
           is=ico(np)
           ncellsp=ncellsp+1
           ijkctmp(ncellsp)=ijkcell(n)
           ijktmp2(ncellsp)=ijkcell(n)
         enddo
!
         if(ncellsp/=0) then
            nomore=.false.
!
         call parlocat (ncellsp, ijkctmp, iphd2, iwid, jwid, kwid, nsampl, &
            vrms, t_wall, ico, ijktmp2, ijktmp3, ijktmp4, rmaj, dz, divpix, &
            area_x, area_y, area_z, dt, itdim, npart, ibar, jbar, kbar, mgeom, &
            cdlt, sdlt, plost_pos, plost_neg, wate, x, y, z, bxv, byv, bzv, &
            xptilde, yptilde, zptilde, uptilde, vptilde, wptilde, tsix, tsiy, &
            tsiz, etax, etay, etaz, nux, nuy, nuz, link, iplost, qpar, px, py, &
            pz, up, vp, wp, pxi, peta, pzta, cartesian, killer, bcpl, bcpr, &
            bcpb, bcpt, bcpe, bcpf, dx, dy, dz, xl, xr, yb, yt, ze, zf, nu_len&
            , nu_comm)
!
        do n=1,ncellsp
           ijk=ijkctmp(n)
           np=iphd2(ijk)
           if(np<=0) cycle
!
           no_inj=no_inj+1
!
           inew=int(pxi(np))
           jnew=int(peta(np))
           knew=int(pzta(np))
!
           jnew=max(jnew,2)
           jnew=min(jnew,jbp1)
           knew=max(knew,2)
           knew=min(knew,kbp1)
!
           ijknew=(knew-1)*kwid+(jnew-1)*jwid+(inew-1)*iwid+1
!
           iphd2(ijk)=link(np)
           link(np)=iphead(ijknew)
           iphead(ijknew)=np
!
         enddo
!
            if(.not.nomore) go to 1
!
         endif
!
        write(*,*) 'ParInject:  no_inj=',no_inj
	return
	end
	
