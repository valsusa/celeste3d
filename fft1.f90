      subroutine modefft1d(ncyc, ibp2, jbp2, kbp2, x, iwid, jwid, kwid, idx, &
         idy, idz, fft, work, work1, zname, gm0, gm1, gm2, gm3, gm4, hm0, hm1, &
         hm2, hm3, hm4) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: ncyc 
      integer , intent(in) :: ibp2 
      integer , intent(in) :: jbp2 
      integer , intent(in) :: kbp2 
      integer , intent(in) :: iwid 
      integer , intent(in) :: jwid 
      integer , intent(in) :: kwid 
      integer  :: idx 
      integer  :: idy 
      integer  :: idz 
      real(double) , intent(out) :: gm0 
      real(double) , intent(out) :: gm1 
      real(double) , intent(out) :: gm2 
      real(double) , intent(out) :: gm3 
      real(double) , intent(out) :: gm4 
      real(double) , intent(out) :: hm0 
      real(double) , intent(out) :: hm1 
      real(double) , intent(out) :: hm2 
      real(double) , intent(out) :: hm3 
      real(double) , intent(out) :: hm4 
      character  :: zname*3 
      real(double) , intent(in) :: x(*) 
      real(double)  :: fft(*) 
      real(double)  :: work(*) 
      real(double) , intent(inout) :: work1(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: ibp1, jbp1, kbp1, n, i, j, k, ijk, l 
      real(double) :: ala 
      character , dimension(2) :: znam2*3 
      character :: name*80 
!-----------------------------------------------
!
      ibp1 = ibp2 - 1 
      jbp1 = jbp2 - 1 
      kbp1 = kbp2 - 1 
!
!     Fourier Analysis in  x
!
      work(:10) = 0. 
      work1(:10) = 0. 
!
      do i = 2, ibp1 
         work(i-1) = 0. 
         do j = 2, jbp1 
            work(i-1) = work(i-1) + sum(x(kwid+1+(i-1)*iwid+(j-1)*jwid:(kbp1-1)&
               *kwid+1+(i-1)*iwid+(j-1)*jwid:kwid)) 
         end do 
      end do 
!
      call fftrf (ibp2 - 2, work, fft) 
!
      l = 0 
      do n = 1, ibp2 - 2, 2 
         if (n == 1) then 
            ala = 0.5*log10(fft(n)**2+1.E-10) 
         else 
            ala = 0.5*log10(fft(n-1)**2+fft(n)**2+1.E-10) 
         endif 
         l = l + 1 
         work1(l) = ala 
         work(l) = n/2 
         l = l + 1 
         work1(l) = ala 
         work(l) = n/2 + 1 - 0.01 
      end do 
      gm0 = work1(1) 
      gm1 = work1(3) 
      gm2 = work1(5) 
      gm3 = work1(7) 
      gm4 = work1(9) 
!
!     write(*,*)'fft',l
!      call plthist(1,l,work,work1,'fft-x')
!
 
!
!     Fourier Analysis in  y
!
      work(:10) = 0. 
      work1(:10) = 0. 
!
      do j = 2, jbp1 
         work(j-1) = 0. 
         do i = 2, ibp1 
            work(j-1) = work(j-1) + sum(x(kwid+1+(i-1)*iwid+(j-1)*jwid:(kbp1-1)&
               *kwid+1+(i-1)*iwid+(j-1)*jwid:kwid)) 
         end do 
!      work(j)=cos(2.*3.14*(j-1)/float(jbp2-1))
      end do 
!
      call fftrf (jbp2 - 2, work, fft) 
!
      l = 0 
      do n = 1, jbp2 - 2, 2 
         if (n == 1) then 
            ala = 0.5*log10(fft(n)**2+1.E-10) 
         else 
            ala = 0.5*log10(fft(n-1)**2+fft(n)**2+1.E-10) 
         endif 
         l = l + 1 
         work1(l) = ala 
         work(l) = n/2 
         l = l + 1 
         work1(l) = ala 
         work(l) = n/2 + 1 - 0.01 
      end do 
      hm0 = work1(1) 
      hm1 = work1(3) 
      hm2 = work1(5) 
      hm3 = work1(7) 
      hm4 = work1(9) 
!
!     write(*,*)'fft',l
!      call plthist(1,l,work,work1,'fft-y')
!
 
!
!     Fourier Analysis in  z
!
      do k = 2, kbp1 
         work(k-1) = 0. 
         do j = 2, jbp1 
            work(k-1) = work(k-1) + sum(x(iwid+1+(j-1)*jwid+(k-1)*kwid:(ibp1-1)&
               *iwid+1+(j-1)*jwid+(k-1)*kwid:iwid)) 
         end do 
      end do 
!
      call fftrf (kbp2 - 2, work, fft) 
!
      l = 0 
      do n = 1, kbp2 - 2, 2 
         if (n == 1) then 
            ala = 0.5*log10(fft(n)**2+1.E-10) 
         else 
            ala = 0.5*log10(fft(n-1)**2+fft(n)**2+1.E-10) 
         endif 
         l = l + 1 
         work1(l) = ala 
         work(l) = n/2 
         l = l + 1 
         work1(l) = ala 
         work(l) = n/2 + 1 - 0.01 
      end do 
!
!     write(*,*)'fft',l
!      call plthist(1,l,work,work1,'fft-z')
!
 
 
      return  
      end subroutine modefft1d 
