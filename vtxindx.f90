!dk vtxindx
      subroutine vtxindx(iwid, jwid, kwid, ijkv) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: iwid 
      integer , intent(in) :: jwid 
      integer , intent(in) :: kwid 
      integer , intent(out) :: ijkv(8) 
!-----------------------------------------------
!
      ijkv(1) = iwid 
      ijkv(2) = iwid + jwid 
      ijkv(3) = jwid 
      ijkv(4) = 0 
      ijkv(5) = iwid + kwid 
      ijkv(6) = iwid + jwid + kwid 
      ijkv(7) = jwid + kwid 
      ijkv(8) = kwid 
!
      return  
      end subroutine vtxindx 
