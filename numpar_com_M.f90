      module numpar_com_M 
      use vast_kind_param, only:  double 
!...Created by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
      integer :: kxi, numit, maxit, nomnit, numitv, maxitv, numgrid 
      real(double), dimension(1) :: com3 
      real(double), dimension(2) :: mu, lam 
      real(double), dimension(24) :: grdbn 
      real(double) :: a0, b0, colamu, omga, eps, t, dt, rdt, dtv, dtc, dtpos, &
         dtvsav, dtx, rdtx, scalsq, dtcsav, anc, omanc, rat, rtnc, omgav, pi, &
         thrd, sxth, gtype, tfrcn, zmutrn, zmulon, epsvac, alp, hpalp, hmalp, &
         trgrd, trglst, dtrg, frrg, c3last, taug, alrg 
      logical :: ndrft, testpar 
      end module numpar_com_M 
