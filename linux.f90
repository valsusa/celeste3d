      subroutine exit(i) 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: i 
!-----------------------------------------------
      return  
      end subroutine exit 


 
      subroutine fftrf(n, seq, coef2) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: n 
      real(double) , intent(in) :: seq(*) 
      real(double) , intent(out) :: coef2(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i, isn 
      real(double), dimension(n) :: vec
      real(double), dimension(10*n) :: w
!-----------------------------------------------
 
	vec(:n) = seq(:n)
	call dffti(n,w)
	call dfftf(n,vec,w)
	coef2(:n)=vec(:n)
      return  
      end subroutine fftrf 


 
      subroutine fft2d(n1, n2, ffta, i1, fftc, i2) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  dble_complex, double 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: n1 
      integer  :: n2 
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      complex(dble_complex) , intent(in) :: ffta(i1,*) 
      complex(dble_complex) , intent(out) :: fftc(i2,*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i, j, isn 
      real(double), dimension(n1,n2) :: a, b 
      complex(dble_complex) :: zi 
!-----------------------------------------------
 
      zi = (0.D0,1.D0)
      do i = 1, n1 
         do j = 1, n2 
            a(i,j) = dble(ffta(i,j)) 
            b(i,j) = -dble(zi*ffta(i,j)) 
         end do 
      end do 
 
      isn = 1 
      call fft_web (a, b, n1*n2, n1, n1, isn) 
      call fft_web (a, b, n1*n2, n2, n1*n2, isn) 
 
      fftc(:n1,:n2) = a(:n1,:n2) + zi*b(:n1,:n2) 
 
      return  
      end subroutine fft2d 
