       subroutine restartout
!
!       A subroutine to dump all memory
!
!-----------------------------------------------
!   M o d u l e s
!-----------------------------------------------
USE vast_kind_param
use corgan_com_M
      use cindex_com_M
      use numpar_com_M
      use cprplt_com_M
      use cophys_com_M
      use ctemp_com_M
      use blcom_com_M
      use objects_com_M
use cplot_com_M
use boundary
!
       open(3,file='restart.dat',form='unformatted',status='unknown')

!     saving module blcom
!
      write(3) npcelx, npcely, npcelz, icoi, &
 	numtot, &
 	npcel_control, &
 	mpert, mtresh, iplost, nplost, &
 	xvi, yvi, zvi, &
 	uvi, vvi, wvi, utordrft, uvip, vvip, &
         wvip, siepx, siepy, siepz, rhr, qom, t_wall, wsr_itg, el_itg, &
 	inject_end,inject_front,inject_left,inject_right,&
  	pert_gem, pert_x, vtresh, qlost, plost_pos, plost_neg, wave_amplitude, &
 	drift, &
	shear, lsplit, lcoal, forcefree, shock, fluid_load,initial_equilibrium, &
	external_current, divergence_cleaning, relativity, &
      	iphd2, &
      	wate, &
       	number, &
      	sie, rho, rhol, &
       	qdnc, &
      	qdnv, &
      	mask, jx0, jy0, jz0, jxtilde, jytilde, &
         jztilde, jxtildet, jytildet, jztildet, pixx, pixy, pixz, piyy, piyz, &
         pizz, pixysp2, piyzsp2, gradxchi, gradychi, gradzchi, divpix, divpiy, &
         divpiz, colx, coly, colz, jmag, j12x, j12y, j12z, joldx, joldy, joldz,&
      	jxs, jys, jzs,&
      	decyk, jdote, qdnc0, &
      	udrft, vdrft, wdrft, bxn, byn, bzn, bxv&
         , byv, bzv, p, gradxb, gradyb, ay, gradzb, absb, &
       	iota, &
       	psi, &
      	delta, &
      	itmax, itsub, &
      	a11, a12, a13, a21, a22, a23, a31, a32&
         , a33, &
      	phipot, potavp,curlx, curly, curlz, ex0, ey0, &
         ez0, exold, eyold, ezold, ex, ey, ez, exavp,eyavp,ezavp,&
         susxx,susxy,susxz,suszx,suszy,suszz,&
      	efnrg, ebnrg, ekenrg, ekenrgx, ekenrgy, ekenrgz, &
         ekinrg, ekinrgx, ekinrgy, ekinrgz, efnrgx, &
         efnrgy, efnrgz, ebnrgx, ebnrgy, ebnrgz, eoenrg, eoinrg, charge, &
         thistry, toroidal_j, citer_maxwell, citer_poisson, costcc, chcons1, &
         chcons2,  &
      	chnorm1, chnorm2, &
      	iphead, ijkcell, ijkvtx, ijktmp2, ijktmp3, &
         ijktmp4, ijkctmp, &
      	istep,&
      	x, y, z, vol, vvol, vvol_half, area_x, &
         area_y, area_z, tsix, tsiy, tsiz, nux, nuy, nuz, etax, etay, etaz , &
      	lama, lamb, lamc, &
      	adaptg, fields, explicit, &
      	c1x, c2x, c3x, c4x, c5x, c6x, c7x, c8x&
         , rxv, c1y, c2y, c3y, c4y, c5y, c6y, c7y, c8y, c1z, c2z, c3z, c4z, c5z&
         , c6z, c7z, c8z, &
      	wgrid, &
      	link, ico, &
      	npsav, &
       	nptotl, npsampl, &
      	px, py, pz, pmu, pxi, peta, pzta, up&
         , vp, wp, qpar, &
      	xptilde, yptilde, zptilde, uptilde, &
         vptilde, wptilde, bxpn, bypn, bzpn, expn, eypn, ezpn , &
      	vrms, proble, &
      	killer, &
      	bcpl, bcpr, bcpt, bcpb, bcpe, bcpf, nfreq, nsmooth, &
      	wkl, wkr,  wke, wkf, xl, xr, yt, yb, ze, zf, &
         elle , &
      	periodicx, periodicz, plots, smoothing !, &
!      	ncyc_restart, &
!        restart

!	saving module corgan

	write(3)iplb1, iplb2, &
      	ipbuff, &
      	cq, &
      	idisp, &
      	krd, kpr, kpt, kfm, kpl, ktpin, ktpout, jbnm, itlm, numtd, &
         itlwd, mgeom, ncr1, ncr2, ncr3, ixto, lpr, itrlmp, ncyc, ncyc1, ncyc2&
         , nmeflm, nflm, &
      	com1, &
      	dto, dtoc, param, &
      	tlm, stim, t1, t2, dcttd, dpttd, pttd, tout, tmovi &
         , pflag, pars, strait, toroid, tlimd, twfin, tt, c1last, cntr, avg,&
      	wrtp, outflg, precon, eandm, anydrift, shape, touch, frctn, &
         periodic, collide, newstuf, nome_file_input, nome_run, ddate,hour

!	saving module cindex
	write(3)ibp, iep, &
      	irt, ibk, &
      	ibar, jbar, kbar, kplas, kvac, kvm, kvp, ibp1, ibp2, jbp1, &
         jbp2, kbp1, kbp2, i, j, k, ipjk, ipjpk, ijpk, ijk, ipjkp, ipjpkp, &
         ijpkp, ijkp, nay, nay2, nayp, kvm2, inx, iper, jper, iperp, jperp, lp&
         , lpm, idtc, jdtc, kdtc, idtv, jdtv, kdtv, iwid, jwid, kwid, &
         ncells, nvtx, nh, nphist, nrg, nvtxkm, ncellsp, &
      	com2, &
      	fibar, fjbar, fkbar, rfibar, rfjbar, rfkbar, rbjbkb, &
         c2last


!	saving module numpar
	write(3)kxi, numit, maxit, nomnit, numitv, maxitv, numgrid , &
      	com3, &
      	mu, lam, &
      	grdbn, &
      	a0, b0, colamu, omga, eps, t, dt, rdt, dtv, dtc, dtpos, &
         dtvsav, dtx, rdtx, scalsq, dtcsav, anc, omanc, rat, rtnc, omgav, pi, &
         thrd, sxth, gtype, tfrcn, zmutrn, zmulon, epsvac, alp, hpalp, hmalp, &
         trgrd, trglst, dtrg, frrg, c3last, taug, alrg , &
      	ndrft, testpar

!	saving module cprplt
	write(3)xphys, yphys, zphys, upar, vpar, wpar, &
         bxpar, bypar, bzpar, mupar, epar

!	saving  module cophys
	write(3)idvb, jdvb, kdvb, &
      	com5, &
      	rwz, &
      gm1, rho0, beta, asq, a, wsa, h, tke, tie, etotl, tmass, &
         tmomx, tmomy, tmomz, tmom, bl0, bl1, bl2, bl3, bl23, bvrt, bl0sv, &
         bl1sv, bl2sv, bl3sv, bvrtsv, tbe, tle, dx, dy, dz, rpl, rvac, rwall, &
         rmaj, cdlhf, sdlhf, wghti, wghtj, elpp, elpt, eltt, zdivb, cdlt, sdlt&
         , cdph, sdph, cdphhf, sdphhf, dphi, tmax, tmax0, tmax1, tmax2, tmax3, &
         rdl0sv, rdl1sv, del2sv, del0, del1, del2, del3, del4, del5, del7, &
         bl23sv, rq0, btorus, bvertcl, rhoi, siei, racc, zacc, tflux, pflux, gx&
         , gy, gz, tiesav, tkesav, tbesav, dvbmax, angv, resist, tangv, omgdt, &
         q0, coll, efld0, c5last, el_rec, bx_ini, by_ini, eps_rec,&
         nu_len, epsil, ex_ini, ey_ini, ez_ini,&
         rnu_rec, bz_ini, ex_ext, ey_ext, ez_ext, ex_const, ey_const, ez_const, &
      	fldivb, cartesian, nu_comm



! 	saving module ctemp_com
	write(3)ivp, &
      	com6, &
      	cx, cy, cz, cpl, &
      	uv, vv, wv, &
      	fxp, fyp, fzp, rsq , &
      	fmf, fef, fvf, fbxf, fbyf, fbzf, &
      	xv, yv, zv, &
      	twx, &
      	uvv, vvv, wvv, xpc, ypc, zpc, &
      	omg


!	saving module objects_com
	write(3)link_obj, ico_obj, &
      	iphead_obj, &
      	npcelx_obj, npcely_obj, npcelz_obj, &
         icoi_obj, &
      	nrg_obj, &
      	px_obj, py_obj, pz_obj, pxi_obj, &
         peta_obj, pzta_obj, chip_obj, massp_obj , &
      	rho_obj, chii_obj, theta1_obj, &
         theta2_obj, phi1_obj, phi2_obj, r_minor1_obj, r_minor2_obj ,&
      	mv, chi, chic

!	saving module cplot
	 write(3)iout, &
     	idnum , &
      	 iplot, cdfid, timdim, idtime, idcyc, idnx, idny, idnz, idu, &
         idv, idw, iddx, iddy, iddz, idrho, idp, idnumtot, idntot_o, idntot_r, &
         idcurlx, idcurly, idcurlz, iddiv, idsie, idpot, idjx, idjy, idjz, &
         idj0x, idj0y, idj0z, idbx, idby, idbz, iddens1, iddens2, idaymin, &
         idaymax, idrfsolar, iday, idex, idey, idez, idux1, iduy1, iduz1, idux2, iduy2, &
         iduz2, idpixysp2, idpiyzsp2, idpixxsp2, idpiyysp2,idpizzsp2, idpixzsp2, &
         idpixysp1, idpiyzsp1, idpixxsp1, idpiyysp1,idpizzsp1, idpixzsp1, &
         idwght, idfx, idfy, idfz, cdfidh, timdimh,&
         idtimeh, idtote, idtoti, idtotk, idtstr, idtelas, idmox, idmoy, &
         idmoz, idxcm, idycm, idzcm, iducm, idvcm, idwcm, idrotx, idroty, &
         idrotz, idrotrx, idrotry, idrotrz, idef, ideb, ideke, idekex, &
         idekey, idekez, ideki, idekix, idekiy, idekiz, ideoe, &
         ideoi, idchrg, idefx, idefy, idefz, idebx, ideby, idebz, ibxn0, ibxn1&
         , ibxn2, ibxn3, ibxn4, ibyn0, ibyn1, ibyn2, ibyn3, ibyn4, ibzn0, ibzn1&
         , ibzn2, ibzn3, ibzn4, iexn0, iexn1, iexn2, iexn3, iexn4, ieyn0, ieyn1&
         , ieyn2, ieyn3, ieyn4, iezn0, iezn1, iezn2, iezn3, iezn4, ijxn0, ijxn1&
         , ijxn2, ijxn3, ijxn4, ijyn0, ijyn1, ijyn2, ijyn3, ijyn4, ijzn0, ijzn1&
         , ijzn2, ijzn3, ijzn4, ibxm0, ibxm1, ibxm2, ibxm3, ibxm4, ibym0, ibym1&
         , ibym2, ibym3, ibym4, ibzm0, ibzm1, ibzm2, ibzm3, ibzm4, iexm0, iexm1&
         , iexm2, iexm3, iexm4, ieym0, ieym1, ieym2, ieym3, ieym4, iezm0, iezm1&
         , iezm2, iezm3, iezm4, ijxm0, ijxm1, ijxm2, ijxm3, ijxm4, ijym0, ijym1&
         , ijym2, ijym3, ijym4, ijzm0, ijzm1, ijzm2, ijzm3, ijzm4, idrmod, &
         idimod, idrmodz, idimodz, ichcons1, ichcons2, cdfidp, idtimep, idcycp&
         , timdimp, idnxp, idnyp, idnzp, idco, idup, idvp, idwp, idqp, idxp, &
         idyp, idzp
!	saving module boundary
	 write(3)periodicy,periodicz,bcMx,bcMy,bcMz,wkb,wkt,bc_vtoctov


       close(unit=3)

       return
       end

       subroutine restartin
!
!       A subroutine to recover all memory

!
       USE vast_kind_param
use corgan_com_M
      use cindex_com_M
      use numpar_com_M
      use cprplt_com_M
      use cophys_com_M
      use ctemp_com_M
      use blcom_com_M
      use objects_com_M
use cplot_com_M
use boundary
!
       open(3,file='restart.dat',form='unformatted',status='unknown')

!     saving module blcom
!
      read(3) npcelx, npcely, npcelz, icoi, &
 	numtot, &
 	npcel_control, &
 	mpert, mtresh, iplost, nplost, &
 	xvi, yvi, zvi, &
 	uvi, vvi, wvi, utordrft, uvip, vvip, &
         wvip, siepx, siepy, siepz, rhr, qom, t_wall, wsr_itg, el_itg, &
 	inject_end,inject_front,inject_left,inject_right,&
  	pert_gem, pert_x, vtresh, qlost, plost_pos, plost_neg, wave_amplitude, &
 	drift, &
	shear, lsplit, lcoal, forcefree, shock, fluid_load,initial_equilibrium, &
		external_current, divergence_cleaning, relativity, &
      	iphd2, &
      	wate, &
       	number, &
      	sie, rho, rhol, &
       	qdnc, &
      	qdnv, &
      	mask, jx0, jy0, jz0, jxtilde, jytilde, &
         jztilde, jxtildet, jytildet, jztildet, pixx, pixy, pixz, piyy, piyz, &
         pizz, pixysp2, piyzsp2, gradxchi, gradychi, gradzchi, divpix, divpiy, &
         divpiz, colx, coly, colz, jmag, j12x, j12y, j12z, joldx, joldy, joldz, &
      	jxs, jys, jzs,&
      	decyk, jdote, qdnc0, &
      	udrft, vdrft, wdrft, bxn, byn, bzn, bxv&
         , byv, bzv, p, gradxb, gradyb, ay, gradzb, absb, &
       	iota, &
       	psi, &
      	delta, &
      	itmax, itsub, &
      	a11, a12, a13, a21, a22, a23, a31, a32&
         , a33, &
      	phipot, potavp,curlx, curly, curlz, ex0, ey0, &
         ez0, exold, eyold, ezold, ex, ey, ez, exavp,eyavp,ezavp,&
         susxx,susxy,susxz,suszx,suszy,suszz,&
      	efnrg, ebnrg, ekenrg, ekenrgx, ekenrgy, ekenrgz, &
         ekinrg, ekinrgx, ekinrgy, ekinrgz, efnrgx, &
         efnrgy, efnrgz, ebnrgx, ebnrgy, ebnrgz, eoenrg, eoinrg, charge, &
         thistry, toroidal_j, citer_maxwell, citer_poisson, costcc, chcons1, &
         chcons2,  &
      	chnorm1, chnorm2, &
      	iphead, ijkcell, ijkvtx, ijktmp2, ijktmp3, &
         ijktmp4, ijkctmp, &
      	istep,&
      	x, y, z, vol, vvol, vvol_half, area_x, &
         area_y, area_z, tsix, tsiy, tsiz, nux, nuy, nuz, etax, etay, etaz , &
      	lama, lamb, lamc, &
      	adaptg, fields, explicit, &
      	c1x, c2x, c3x, c4x, c5x, c6x, c7x, c8x&
         , rxv, c1y, c2y, c3y, c4y, c5y, c6y, c7y, c8y, c1z, c2z, c3z, c4z, c5z&
         , c6z, c7z, c8z, &
      	wgrid, &
      	link, ico, &
      	npsav, &
       	nptotl, npsampl, &
      	px, py, pz, pmu, pxi, peta, pzta, up&
         , vp, wp, qpar, &
      	xptilde, yptilde, zptilde, uptilde, &
         vptilde, wptilde, bxpn, bypn, bzpn, expn, eypn, ezpn , &
      	vrms, proble, &
      	killer, &
      	bcpl, bcpr, bcpt, bcpb, bcpe, bcpf, nfreq, nsmooth, &
      	wkl, wkr,  wke, wkf, xl, xr, yt, yb, ze, zf, &
         elle , &
      	periodicx, periodicz, plots, smoothing

!	saving module corgan

	read(3)iplb1, iplb2, &
      	ipbuff, &
      	cq, &
      	idisp, &
      	krd, kpr, kpt, kfm, kpl, ktpin, ktpout, jbnm, itlm, numtd, &
         itlwd, mgeom, ncr1, ncr2, ncr3, ixto, lpr, itrlmp, ncyc, ncyc1, ncyc2&
         , nmeflm, nflm, &
      	com1, &
      	dto, dtoc, param, &
      	tlm, stim, t1, t2, dcttd, dpttd, pttd, tout, tmovi &
         , pflag, pars, strait, toroid, tlimd, twfin, tt, c1last, cntr, avg,&
      	wrtp, outflg, precon, eandm, anydrift, shape, touch, frctn, &
         periodic, collide, newstuf, nome_file_input, nome_run, ddate,hour

!	saving module cindex
	read(3)ibp, iep, &
      	irt, ibk, &
      	ibar, jbar, kbar, kplas, kvac, kvm, kvp, ibp1, ibp2, jbp1, &
         jbp2, kbp1, kbp2, i, j, k, ipjk, ipjpk, ijpk, ijk, ipjkp, ipjpkp, &
         ijpkp, ijkp, nay, nay2, nayp, kvm2, inx, iper, jper, iperp, jperp, lp&
         , lpm, idtc, jdtc, kdtc, idtv, jdtv, kdtv, iwid, jwid, kwid, &
         ncells, nvtx, nh, nphist, nrg, nvtxkm, ncellsp, &
      	com2, &
      	fibar, fjbar, fkbar, rfibar, rfjbar, rfkbar, rbjbkb, &
         c2last


!	saving module numpar
	read(3)kxi, numit, maxit, nomnit, numitv, maxitv, numgrid , &
      	com3, &
      	mu, lam, &
      	grdbn, &
      	a0, b0, colamu, omga, eps, t, dt, rdt, dtv, dtc, dtpos, &
         dtvsav, dtx, rdtx, scalsq, dtcsav, anc, omanc, rat, rtnc, omgav, pi, &
         thrd, sxth, gtype, tfrcn, zmutrn, zmulon, epsvac, alp, hpalp, hmalp, &
         trgrd, trglst, dtrg, frrg, c3last, taug, alrg , &
      	ndrft, testpar

!	saving module cprplt
	read(3)xphys, yphys, zphys, upar, vpar, wpar, &
         bxpar, bypar, bzpar, mupar, epar

!	saving  module cophys
	read(3)idvb, jdvb, kdvb, &
      	com5, &
      	rwz, &
        gm1, rho0, beta, asq, a, wsa, h, tke, tie, etotl, tmass, &
         tmomx, tmomy, tmomz, tmom, bl0, bl1, bl2, bl3, bl23, bvrt, bl0sv, &
         bl1sv, bl2sv, bl3sv, bvrtsv, tbe, tle, dx, dy, dz, rpl, rvac, rwall, &
         rmaj, cdlhf, sdlhf, wghti, wghtj, elpp, elpt, eltt, zdivb, cdlt, sdlt&
         , cdph, sdph, cdphhf, sdphhf, dphi, tmax, tmax0, tmax1, tmax2, tmax3, &
         rdl0sv, rdl1sv, del2sv, del0, del1, del2, del3, del4, del5, del7, &
         bl23sv, rq0, btorus, bvertcl, rhoi, siei, racc, zacc, tflux, pflux, gx&
         , gy, gz, tiesav, tkesav, tbesav, dvbmax, angv, resist, tangv, omgdt, &
         q0, coll, efld0, c5last, el_rec, bx_ini, by_ini, eps_rec,&
         nu_len, epsil, ex_ini, ey_ini, ez_ini,&
         rnu_rec, bz_ini, ex_ext, ey_ext, ez_ext, ex_const, ey_const, ez_const, &
      	fldivb, cartesian, nu_comm



! 	saving module ctemp_com
	read(3)ivp, &
      	com6, &
      	cx, cy, cz, cpl, &
      	uv, vv, wv, &
      	fxp, fyp, fzp, rsq , &
      	fmf, fef, fvf, fbxf, fbyf, fbzf, &
      	xv, yv, zv, &
      	twx, &
      	uvv, vvv, wvv, xpc, ypc, zpc, &
      	omg


!	saving module objects_com
	read(3)link_obj, ico_obj, &
      	iphead_obj, &
      	npcelx_obj, npcely_obj, npcelz_obj, &
         icoi_obj, &
      	nrg_obj, &
      	px_obj, py_obj, pz_obj, pxi_obj, &
         peta_obj, pzta_obj, chip_obj, massp_obj , &
      	rho_obj, chii_obj, theta1_obj, &
         theta2_obj, phi1_obj, phi2_obj, r_minor1_obj, r_minor2_obj ,&
      	mv, chi, chic



!	saving module cplot
	 read(3)iout, &
     	idnum , &
      	 iplot, cdfid, timdim, idtime, idcyc, idnx, idny, idnz, idu, &
         idv, idw, iddx, iddy, iddz, idrho, idp, idnumtot, idntot_o, idntot_r, &
         idcurlx, idcurly, idcurlz, iddiv, idsie, idpot, idjx, idjy, idjz, &
         idj0x, idj0y, idj0z, idbx, idby, idbz, iddens1, iddens2, idaymin, &
         idaymax, idrfsolar, iday, idex, idey, idez, idux1, iduy1, iduz1, idux2, iduy2, &
         iduz2, idpixysp2, idpiyzsp2, idpixxsp2, idpiyysp2,idpizzsp2, idpixzsp2, &
         idpixysp1, idpiyzsp1, idpixxsp1, idpiyysp1,idpizzsp1, idpixzsp1, &
         idwght, idfx, idfy, idfz, cdfidh, timdimh,&
         idtimeh, idtote, idtoti, idtotk, idtstr, idtelas, idmox, idmoy, &
         idmoz, idxcm, idycm, idzcm, iducm, idvcm, idwcm, idrotx, idroty, &
         idrotz, idrotrx, idrotry, idrotrz, idef, ideb, ideke, idekex, &
         idekey, idekez, ideki, idekix, idekiy, idekiz, ideoe, &
         ideoi, idchrg, idefx, idefy, idefz, idebx, ideby, idebz, ibxn0, ibxn1&
         , ibxn2, ibxn3, ibxn4, ibyn0, ibyn1, ibyn2, ibyn3, ibyn4, ibzn0, ibzn1&
         , ibzn2, ibzn3, ibzn4, iexn0, iexn1, iexn2, iexn3, iexn4, ieyn0, ieyn1&
         , ieyn2, ieyn3, ieyn4, iezn0, iezn1, iezn2, iezn3, iezn4, ijxn0, ijxn1&
         , ijxn2, ijxn3, ijxn4, ijyn0, ijyn1, ijyn2, ijyn3, ijyn4, ijzn0, ijzn1&
         , ijzn2, ijzn3, ijzn4, ibxm0, ibxm1, ibxm2, ibxm3, ibxm4, ibym0, ibym1&
         , ibym2, ibym3, ibym4, ibzm0, ibzm1, ibzm2, ibzm3, ibzm4, iexm0, iexm1&
         , iexm2, iexm3, iexm4, ieym0, ieym1, ieym2, ieym3, ieym4, iezm0, iezm1&
         , iezm2, iezm3, iezm4, ijxm0, ijxm1, ijxm2, ijxm3, ijxm4, ijym0, ijym1&
         , ijym2, ijym3, ijym4, ijzm0, ijzm1, ijzm2, ijzm3, ijzm4, idrmod, &
         idimod, idrmodz, idimodz, ichcons1, ichcons2, cdfidp, idtimep, idcycp&
         , timdimp, idnxp, idnyp, idnzp, idco, idup, idvp, idwp, idqp, idxp, &
         idyp, idzp

!	saving module boundary
	 read(3)periodicy,periodicz,bcMx,bcMy,bcMz,wkb,wkt,bc_vtoctov

       close(unit=3)

       return
       end
