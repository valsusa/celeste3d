      module ctemp_com_M 
      use vast_kind_param, only:  double 
!...Created by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
      use corgan_com_M, only: ijmx 
      integer, dimension(8) :: ivp 
      real(double), dimension(1) :: com6 
      real(double), dimension(8) :: cx, cy, cz, cpl 
      real(double), dimension(20) :: uv, vv, wv 
      real(double), dimension(8) :: fxp, fyp, fzp, rsq 
      real(double), dimension(100) :: fmf, fef, fvf, fbxf, fbyf, fbzf 
      real(double), dimension(20) :: xv, yv, zv 
      real(double), dimension(9000) :: twx 
      real(double), dimension(ijmx) :: uvv, vvv, wvv, xpc, ypc, zpc 
      real(double) :: omg 
      end module ctemp_com_M 
