      subroutine smooth(i1, i2, j1, j2, k1, k2, iwid, jwid, kwid, nsm, chic, &
         dchic, periodicx) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: i1 
      integer  :: i2 
      integer  :: j1 
      integer  :: j2 
      integer  :: k1 
      integer  :: k2 
      integer  :: iwid 
      integer  :: jwid 
      integer  :: kwid 
      integer , intent(in) :: nsm 
      logical  :: periodicx 
      real(double)  :: chic(*) 
      real(double) , intent(inout) :: dchic(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: ns, k, j, i, ijk 
      real(double) :: sixth, wsp, chi_avg 
!-----------------------------------------------
!
!     a routine to smooth a function
!
!      sixth=1./6.
      sixth = 1./4.5 
!
      call torusbc_scal (i1, i2, j1, j2, k1, k2, iwid, jwid, kwid, wsp, chic, &
         periodicx) 
!
      do ns = 1, nsm 
!
         do k = k1 + 1, k2 - 1 
            do j = j1, j2 - 1 
               do i = i1, i2 - 1 
!
                  ijk = (k - 1)*kwid + (j - 1)*jwid + (i - 1)*iwid + 1 
!
                  chi_avg = ((chic(ijk+iwid)+chic(ijk-iwid))/4.+chic(ijk+jwid)+&
                     chic(ijk-jwid)+chic(ijk+kwid)+chic(ijk-kwid))*sixth 
!
                  dchic(ijk) = chi_avg - chic(ijk) 
!
               end do 
            end do 
         end do 
!
         do k = k1 + 1, k2 - 1 
            do j = j1, j2 - 1 
!
               chic(iwid*(i1-1)+(k-1)*kwid+(j-1)*jwid+1:(i2-2)*iwid+(k-1)*kwid+&
                  (j-1)*jwid+1:iwid) = chic(iwid*(i1-1)+(k-1)*kwid+(j-1)*jwid+1&
                  :(i2-2)*iwid+(k-1)*kwid+(j-1)*jwid+1:iwid) + 0.4*dchic(iwid*(&
                  i1-1)+(k-1)*kwid+(j-1)*jwid+1:(i2-2)*iwid+(k-1)*kwid+(j-1)*&
                  jwid+1:iwid) 
!
            end do 
         end do 
!
!      do 13 j=j1,j2-1
!      do 13 i=i1,i2-1
!c
!      ijk=(k2-1)*kwid+(j-1)*jwid+(i-1)*iwid+1
!c
!      chic(ijk-kwid)=chic(ijk)
!c
!   13 continue
!
         wsp = 0. 
!
         call torusbc_scal (i1, i2, j1, j2, k1, k2, iwid, jwid, kwid, wsp, chic&
            , periodicx) 
!
      end do 
!
      return  
      end subroutine smooth 
