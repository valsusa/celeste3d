      module cindex_com_M
      use vast_kind_param, only:  double
!...Created by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02
      integer, dimension(10) :: ibp, iep
      integer, dimension(8) :: irt, ibk
      integer :: ibar, jbar, kbar, kplas, kvac, kvm, kvp, ibp1, ibp2, jbp1, &
         jbp2, kbp1, kbp2, i, j, k, ipjk, ipjpk, ijpk, ijk, ipjkp, ipjpkp, &
         ijpkp, ijkp, nay, nay2, nayp, kvm2, inx, iper, jper, iperp, jperp, lp&
         , lpm, idtc, jdtc, kdtc, idtv, jdtv, kdtv, iwid, jwid, kwid, &
         ncells, nvtx, nh, nphist, nrg, nvtxkm, ncellsp
      real(double), dimension(1) :: com2
      real(double) :: fibar, fjbar, fkbar, rfibar, rfjbar, rfkbar, rbjbkb, &
         c2last
      end module cindex_com_M
