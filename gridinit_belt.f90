!dk gridinit
      subroutine gridinit_belt(ibar, jbar, kbar, iwid, jwid, kwid, delt, dphi, &
         dtheta, dx, dy, dz, rwall, rmaj, dzstr, istep, del1, del2, del3, del4&
         , del5, del6, del7, x, y, z, thetan, cartesian, nu_len, nu_comm) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: ibar 
      integer  :: jbar 
      integer  :: kbar 
      integer  :: iwid 
      integer  :: jwid 
      integer  :: kwid 
      real(double)  :: delt 
      real(double) , intent(in) :: dphi 
      real(double) , intent(in) :: dtheta 
      real(double)  :: dx 
      real(double)  :: dy 
      real(double)  :: dz 
      real(double)  :: rwall 
      real(double) , intent(in) :: rmaj 
      real(double) , intent(in) :: dzstr 
      real(double) , intent(in) :: del1 
      real(double) , intent(in) :: del2 
      real(double) , intent(in) :: del3 
      real(double) , intent(in) :: del4 
      real(double) , intent(in) :: del5 
      real(double)  :: del6 
      real(double) , intent(in) :: del7 
      real(double)  :: nu_len 
      logical , intent(in) :: cartesian 
      logical  :: nu_comm 
      integer , intent(in) :: istep(*) 
      real(double)  :: x(*) 
      real(double)  :: y(*) 
      real(double)  :: z(*) 
      real(double) , intent(in) :: thetan(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: k, j, i, ijk 
      real(double) :: rfibar, rfjbar, rfkbar, pi, dtdj, dt0, phiang, theta, &
         theta0, delr, rr, ws, xx, yy 
!-----------------------------------------------
!
!     impose a twist on the mesh
!     one rotation over the length of the mesh in j
!
      if (cartesian) then 
         call gridinit_cart (ibar, jbar, kbar, iwid, jwid, kwid, dx, dy, dz, x&
            , y, z, nu_len, nu_comm) 
         return  
      endif 
 
      rfibar = 1./float(ibar) 
      rfjbar = 1./float(jbar) 
      rfkbar = 1./float(kbar) 
!
      pi = acos(-1.) 
!
      do k = 1, kbar + 2 
!
         dtdj = float(istep(k))*dtheta*rfjbar 
         dt0 = -0.5*float(istep(k))*dtheta 
!
         do j = 1, jbar + 2 
!
            phiang = (j - 2)*dphi 
!
            do i = 1, ibar + 2 
!
               ijk = (k - 1)*kwid + (j - 1)*jwid + (i - 1)*iwid + 1 
!
!      theta=(i-2)*dtheta+(j-2)*dtdj+dt0
               theta = thetan(i) 
!
!     calculate the wall for the jet torus
!
               call jet (theta, rwall) 
!
               theta0 = theta - 0.5*pi 
               delr = rwall*rfkbar*(1. + del1*cos(theta0) + del2*cos(2.*(theta0&
                   + 0.5*pi)) + del3*cos(3.*theta0) + del4*cos(4.*theta0) + &
                  del5*cos(5.*theta0) + del7*cos(7.*theta0)) 
!
               rr = (k - 2)*delr 
               ws = rmaj + rr*sin(theta) 
               xx = ws*cos(phiang) 
               x(ijk) = xx 
               yy = ws*sin(phiang) + float(j - 2)*dzstr 
               y(ijk) = yy 
               z(ijk) = rr*cos(theta) 
!
            end do 
!
         end do 
!
      end do 
!
!
      return  
      end subroutine gridinit_belt 
