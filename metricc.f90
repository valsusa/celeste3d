      subroutine metricc(ncells, ijkcell, c1x, c2x, c3x, c4x, c5x, c6x, c7x, &
         c8x, c1y, c2y, c3y, c4y, c5y, c6y, c7y, c8y, c1z, c2z, c3z, c4z, c5z, &
         c6z, c7z, c8z, vol, tsix, tsiy, tsiz, etax, etay, etaz, nux, nuy, nuz) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: ncells 
      integer , intent(in) :: ijkcell(*) 
      real(double) , intent(in) :: c1x(*) 
      real(double) , intent(in) :: c2x(*) 
      real(double) , intent(in) :: c3x(*) 
      real(double)  :: c4x(*) 
      real(double) , intent(in) :: c5x(*) 
      real(double) , intent(in) :: c6x(*) 
      real(double) , intent(in) :: c7x(*) 
      real(double) , intent(in) :: c8x(*) 
      real(double) , intent(in) :: c1y(*) 
      real(double) , intent(in) :: c2y(*) 
      real(double) , intent(in) :: c3y(*) 
      real(double)  :: c4y(*) 
      real(double) , intent(in) :: c5y(*) 
      real(double) , intent(in) :: c6y(*) 
      real(double) , intent(in) :: c7y(*) 
      real(double) , intent(in) :: c8y(*) 
      real(double) , intent(in) :: c1z(*) 
      real(double) , intent(in) :: c2z(*) 
      real(double) , intent(in) :: c3z(*) 
      real(double)  :: c4z(*) 
      real(double) , intent(in) :: c5z(*) 
      real(double) , intent(in) :: c6z(*) 
      real(double) , intent(in) :: c7z(*) 
      real(double) , intent(in) :: c8z(*) 
      real(double) , intent(in) :: vol(*) 
      real(double) , intent(out) :: tsix(*) 
      real(double) , intent(out) :: tsiy(*) 
      real(double) , intent(out) :: tsiz(*) 
      real(double) , intent(out) :: etax(*) 
      real(double) , intent(out) :: etay(*) 
      real(double) , intent(out) :: etaz(*) 
      real(double) , intent(out) :: nux(*) 
      real(double) , intent(out) :: nuy(*) 
      real(double) , intent(out) :: nuz(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: n, ijk 
      real(double) :: rvol 
!-----------------------------------------------
!
!
      do n = 1, ncells 
!
         ijk = ijkcell(n) 
!
         rvol = 1./vol(ijk) 
!
         tsix(ijk) = (c1x(ijk)+c2x(ijk)+c5x(ijk)+c6x(ijk))*rvol 
         tsiy(ijk) = (c1y(ijk)+c2y(ijk)+c5y(ijk)+c6y(ijk))*rvol 
         tsiz(ijk) = (c1z(ijk)+c2z(ijk)+c5z(ijk)+c6z(ijk))*rvol 
!
         etax(ijk) = (c2x(ijk)+c3x(ijk)+c6x(ijk)+c7x(ijk))*rvol 
         etay(ijk) = (c2y(ijk)+c3y(ijk)+c6y(ijk)+c7y(ijk))*rvol 
         etaz(ijk) = (c2z(ijk)+c3z(ijk)+c6z(ijk)+c7z(ijk))*rvol 
!
         nux(ijk) = (c5x(ijk)+c6x(ijk)+c7x(ijk)+c8x(ijk))*rvol 
         nuy(ijk) = (c5y(ijk)+c6y(ijk)+c7y(ijk)+c8y(ijk))*rvol 
         nuz(ijk) = (c5z(ijk)+c6z(ijk)+c7z(ijk)+c8z(ijk))*rvol 
!
      end do 
!
      return  
      end subroutine metricc 
