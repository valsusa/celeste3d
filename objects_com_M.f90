      module objects_com_M 
      use vast_kind_param, only:  double 
!...Created by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
      use corgan_com_M, only: npart_obj 
      use corgan_com_M, only: idx 
      use corgan_com_M, only: idxg, idy, idyg, idz, idzg, lay, itdim, nreg_obj 
      integer, dimension(0:npart_obj) :: link_obj, ico_obj 
      integer, dimension(itdim) :: iphead_obj 
      integer, dimension(nreg_obj) :: npcelx_obj, npcely_obj, npcelz_obj, &
         icoi_obj 
      integer :: nrg_obj 
      real(double), dimension(0:npart_obj) :: px_obj, py_obj, pz_obj, pxi_obj, &
         peta_obj, pzta_obj, chip_obj, massp_obj 
      real(double), dimension(nreg_obj) :: rho_obj, chii_obj, theta1_obj, &
         theta2_obj, phi1_obj, phi2_obj, r_minor1_obj, r_minor2_obj 
      real(double), dimension(itdim) :: mv, chi, chic 
      end module objects_com_M 
