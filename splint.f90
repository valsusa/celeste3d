!
!***********************************************************************
!
      subroutine splint(xa, ya, y2a, n, x, y) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use modify_com_M 
 
!...Translated by Pacific-Sierra Research 77to90  4.3E  14:13:36   8/20/02  
!...Switches: -yf -x1             
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: n 
      real(double) , intent(in) :: xa(*) 
      real(double) , intent(in) :: ya(*) 
      real(double) , intent(in) :: y2a(*) 
      real(double) , intent(in) :: x(*) 
      real(double) , intent(out) :: y(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i, klo, khi, k 
      real(double) :: h, a, b 
!-----------------------------------------------
!
!=======================================================================
!
      do i = 1, n 
         klo = 1 
         khi = n 
    1    continue 
         if (khi - klo > 1) then 
            k = (khi + klo)/2 
            if (xa(k) > x(i)) then 
               khi = k 
            else 
               klo = k 
            endif 
            go to 1 
         endif 
         h = xa(khi) - xa(klo) 
         a = (xa(khi)-x(i))/h 
         b = (x(i)-xa(klo))/h 
         y(i) = a*ya(klo) + b*ya(khi) + ((a**3 - a)*y2a(klo)+(b**3-b)*y2a(khi))&
            *h**2/6. 
      end do 
      return  
      end subroutine splint 
